ALL: README.pdf README.html

README.pdf : README.md
	pandoc $< -o $@

README.html : README.md
	pandoc $< -o $@

okular: README.pdf
	okular $<&

cylcle1 : pdf/cycles/ensel712_annexe_1312894.pdf
	okular $<&

cylcle2 : pdf/cycles/ensel714_annexe1_1312885.pdf
	okular $<&

cylcle3 : pdf/cycles/ensel714_annexe2_1312887.pdf
	okular $<&


jury3eme: pdf/capes/rapport_jury_3econcours_2020.pdf
	okular $<&

jury: pdf/capes/rapport_jury_capes_cafep_2020.pdf
	okular $<&

progNSI1ere: pdf/capes/nsi1ere.pdf
	okular $<&

progNSIterm: pdf/capes/nsiterm.pdf
	okular $<&

progSNT: pdf/capes/snt.pdf
	okular $<&

lecons: pdf/capes/liste_lecons_2021.pdf
	okular $<&

convocation: pdf/capes/Convocation_-_9117116998_-_RISBOURG_SAMUEL.pdf
	okular $<&

sujet0: pdf/capes/2019-Sujet_0._epreuve_2.pdf
	okular $<&

epreuve1_2020: pdf/capes/2020_epreuve_1.pdf pdf/capes/Elements-correction-sujet1-1.pdf
	okular $^&

epreuve2_2020: pdf/capes/2020_epreuve_2.pdf pdf/capes/Elements-correction-sujet2.pdf
	okular $^&

epreuve1_2021: pdf/capes/2021_epreuve_1.pdf
	okular $<&

epreuve2_2021: pdf/capes/2021_epreuve_2.pdf
	okular $<&

algo_college: pdf/maths/26-Maths-C4-reperes-eduscol_1114756.pdf
	okular $<& -p 13

techno_college: pdf/techno/1421-technologie-prg.pdf
	okular $<&

clean:
	@rm -f README.pdf
	@rm -f README.html


.PHONY: README.pdf README.html