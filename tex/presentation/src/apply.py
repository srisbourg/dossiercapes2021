# Cas Tactique apply
if tagt == 'apply':
 if valt in ctx.keys():
  phi =  ctx[valt]
  try:
   return list(map(lambda ff : mk_goal(ctx, ff),\
    conditions_apply(phi, formule)))