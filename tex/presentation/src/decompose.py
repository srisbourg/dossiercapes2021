# Cas Tactique decompose
  if tagt == 'decompose':
    if valt in ctx.keys() :
      tag, val =  ctx[valt]
      if tag == 'Et':
        phi1, phi2 = val
        num1 = next(gen_fresh_ident)
        num2 = next(gen_fresh_ident)
        return [mk_goal({num1:phi1, num2:phi2} | ctx, formule)]