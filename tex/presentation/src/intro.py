# Cas Tactique intro
  if tagt == 'intro' and tagf == 'Implique':
    if valid_ident(valt, ctx):
      formule1, formule2 = valf
      return [mk_goal({valt:formule1} | ctx, formule2)]