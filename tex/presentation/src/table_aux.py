def table_aux(sous_forms, cas_possible, acc=[]):
 if cas_possible == []: # Cas de base
  return acc
 # Appel recursif
 head, *tail = cas_possible
 ret = list(map(lambda f : eval_tf(head, f), sous_forms))
 acc.append(ret)
 return table_aux(sous_forms, tail, acc)  