def tapis (liste_coords_taille,\
 profondeur):
 # Cas de base
  if profondeur == 0 or\
   len(liste_coords_taille) == 0:
    return liste_coords_taille
 # Enfants de la tete de liste
  head, *tail = liste_coords_taille
  x, y, taille = head
  enfants = carres_enfants(x, y,\
   taille)
 # Cas Recursif
  return [head] +\
   tapis(tail, profondeur) +\   
    tapis(enfants, profondeur-1)
  