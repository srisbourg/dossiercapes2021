def table(tformule):
 list_dic = enumerate_case(set_variables(tformule))# Combinaisons possibles
 sous_forms = list(sous_formules(tformule)) # Sous formules
 sous_forms.sort(key=profondeur) # Tri formules
 table_val = table_aux(sous_forms, list(list_dic)) # Liste Lignes
 table_val.sort(key = sort_val) # Tri lignes
 ... # Conversion des valeurs booleenes en "Vrai" et "Faux"
 return [list(map(lambda f : string_of_formule(f), sous_forms))]+  ret