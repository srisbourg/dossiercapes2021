def carres_enfants(x, y, taille):
 ret = []
 ret.append(carre_est(x, y, taille))
 ret.append(carre_nord_est(x, y, taille))
 ret.append(carre_nord(x, y, taille))
 ret.append(carre_nord_ouest(x, y, taille))
 ret.append(carre_ouest(x, y, taille))
 ret.append(carre_sud_ouest(x, y, taille))
 ret.append(carre_sud(x, y, taille))
 ret.append(carre_sud_est(x, y, taille))
 return ret