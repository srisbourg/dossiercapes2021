import sys
import os

sys.path.insert(0, '../../../../pyticoq/lib/')


from type_formule import *
from syntax_formule import *
from printer import *
#from unit_test_common import *
from type_tactic import *
from type_goal import *

p = "(Samuel ==> PasMal) ==> (Pyticoq ==> Interessant) ==> (Enseignement ==> Super) ==> Genial"
s = string_of_formule(analyse_prop(p))
#print(s)

context = { 'h0' : analyse_prop('Samuel'),\
            'h1' : analyse_prop('Samuel ==> Pyticoq'),\
            'h2' : analyse_prop('Samuel ==> Enseignant')    
            }

#print(string_of_context(context))

goal = mk_goal(context, analyse_prop('Laureat \/ ~Laureat'))

print(string_of_goal(goal))