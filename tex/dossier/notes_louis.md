Je n'ai pas d'avis sur la pertinence du sujet en soi, ni du niveau de
celui-ci, vu que je ne sais pas ce qui est attendu exactement

* Typos / grammaire / …

- **DONE** page 1 colonne 2 : Les figures 1 et 2 présente → *présentent*

- page 2 colonne 1 : le résultat de la règle d'inférence devrait être aligné
  au centre + on met pas de =,= habituellement entre les prémisses

- **DONE** "Pour prouver /f2/ dans Γ, il faut […]" → *il suffit de*

-**DONE** l'ensemble des hypothèses présents → l'ensemble des hypothèses *présentes*
  /ou/ l'ensemble *d'* hypothèses présent (je préfère le premier)

- page 2 colonne 2 : considérant que → *étant donné que* ?

- **DONE** page 2 colonne 2 : nous présentons dans cette partie, → enlever la =,=

- **DONE** page 3 colonne 1 : les test conditionnel → les *tests* (et le
  "conditionnel" me semble un peu redondant)

- **DONE** page 3 colonne 1 : à la l'introduction → à +la+ l'introduction

- **DONE** page 3 colonne 1 : quand on la voie → *voit*

- **DONE** page 3 colonne 1 : et que l'on réalise → *se rend compte de* (réalise est
  un anglicisme. Tu peux laisser si tu veux, c'est plus ou moins passé dans
  le langage courant, mais normalement réaliser veut dire faire quelque
  chose)

- **DONE** page 3 colonne 1 : et des machines . → enlever l'espace avant le =.=

- **DONE** page 3 colonne 2 : sur les les définitions → sur les +les+ définitions

- **DONE** page 3 colonne 2 : du ou des cas récursif → *récursifs*

- **DONE** page 3 colonne 2 : sont insérés → *insérées*

- **DONE** page 4 colonne 2 : technicien puis comme ingénieur → technicien puis
  +comme+ ingénieur

* Remarque de fond

- c'est toi qui a écrit pyticoq n'est-ce pas ? Pourquoi utiliser un
  dictionnaire avec seulement deux clés (contexte et formule). Pourquoi pas
  une paire (ou un objet avec deux attributs si tu veux plus de clarté) ?

* Concernant le projet

Mes remarques se basent sur la version jointe au PDF et pas sur la version
gitlab, désolé si certaines choses ont déjà été corrigées

** README

- ~make~ ne fait rien chez moi, il y a une raison ? Sinon on pourrait le
  rediriger sur ~make run~ par exemple ?

- **DONE** l'image ~img/pytikok.jpg~ n'est pas jointe dans le fichier tar.gz, et donc
  pas visible dans le README.md (mais elle est bien dans le pdf)

- **DONE** ligne 100 du README, le \\ n'est pas échappé (et à plein d'autres
  endroits). Du coup le ∧ et le ∨ apparaissent pareil ce qui est pas génial

- **DONE** page 9 du README.pdf : Progammation → *Programmation*

- **DONE** même page : Vrai et Faux ne sont pas formattés pareil

- c'est moi ou il y a pas de tactique pour prouver ~True~ ?

- **DONE** En plus il y a un ordre entre les fichiers (puisqu'ils sont numérotés),
  peut-être que tu peux documenter en mettant un fichier README dans ~lib~ ?

** Analyse du code

De manière générale tu gagnerais à relire tes commentaires et corriger les
typos

*** ~lib/syntax_common.py~  (les lignes correspondent au fichier du dossier lib)
- ligne 15: Tu prétends que ta fonction a un paramètre *l*
  mais c'est faux
- l18  Fasle → *False*
- **DONE** l31  debut mot → ~debut_mot~
- l34  que l'on ignore pas → que l'on *n'* ignore pas
- **DONE** l29  Je ne comprends pas l'intérêt de l'argument ~n~.
- **DONE** l46  Je ne comprends pas l'intérêt de l'argument ~n~ non plus
- l63  deformule → de formule
- **DONE** l75  toujours la même question sur l'intérêt de ~n~ (il y a peut-être la
  même question à d'autres endroits mais je ne le mentionnerai plus)
- l77  le commentaire est pas très clair
- l79  la fonction ne retourne pas un triplet contrairement à ce que tu
  prétends
- l97  alphètique → *alphabétique*
- **DONE** l102  tu peux résumer ~c.isalpha() or c.isdigit()~ en ~c.isalnum()~

Après j'ai pas eu la foi de regarder sérieusement tout le code mais je peux
si tu estimes ça utile

Dans ~type_formule.py~, lignes 64 et 78, ce serait pas plutôt ~return False~ ?
Pareil pour ~type_goal.py~ lignes 43 et 48
