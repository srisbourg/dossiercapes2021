# Remarques liminaires
- Je ne sais pas à quel point tu es coincé par la taille. Je t'ai fait un certain nombre de suggestions, à toi de voir si tu veux/peux en tenir compte.
- **La section 2 n'est pas un peu trop longue** (par rapport à ce qui est attendu du dossier qui a l'air surtout de s'attacher au côté pédagogique) ? <BR/>
**Si ==> TODO reprise des parties de la section 2 dans la 3 pour struturer la présentation des exploitations pédagogiques**
- J'ai noté un certain nombre de typos, du coup mes remarques font un peu catalogue. J'espère que ça ne sera pas trop pénible à lire, et que ça ne noiera pas les quelques remarques que j'ai faites sur le fond (il n'y en a pas beaucoup dans le total).<BR/>
**Juste merci de t'être acquité de cette tache laborieuse!**

# Remarques générales
1. partie 2 : elle manque un peu de liant, je trouve. Il ne manque pas grand chose, je pense que ça peut être l'affaire d'une ou deux phrases.
* Au départ, **tu mets "développement dirigé par les types" sans aucune explication, ça fait un peu bizarre**.
* Ensuite, les deux premiers § s'enchaînent bien (Syntaxe abstraite et Typage), parce qu'on suit ton fil rouge des différences entre OCaml et Python. Mais du coup, quand la suite (Règle logique) arrive, on a perdu le fil de départ qui était tout simplement de présenter les principaux aspects de Pyticoq, et on ne comprend pas bien l'enchaînement.
* Je ne sais pas bien ce qu'il faut retoucher, mais je pense qu'il y a moyen que le cheminement soit plus clair pour le lecteur.
* Il y a aussi peut-être un problème de lisibilité de la portée des titres des paragraphes. Par ex., juste avant "Qualité logicielle", je pensais que le § était rattaché à "Programmation récursive", du coup je ne comprenais pas de quelles fonctions on parlait dans le 1er § de "Qualité logicielle" que je voyais comme un nouveau bloc un peu indépendant. Peut-être que tu voulais que ce § soit un § de transitition, d'ailleurs en relisant, on finit par comprendre, mais je ne sais pas trop expliquer pourquoi, ça m'a semblé confus à la première lecture. Je ne sais pas si ça t'aide. Au pire, attends d'avoir le regard des autres.


2. partie 3 : 1er § vraiment très clair et synthétique, bravo ! Mais je m'interroge : je pensais en lisant les consignes (que je trouve assez peu claires soit dit en passant) que "extraire les exploitations pédagogiques" d'un projet consistait à réutiliser des briques (de code, ou autres) ; tu sembles avoir interprété ça de manière plus large, comme, disons, servir d'inspiration à des activités pédagogiques. Je pense que tu sais mieux que moi ce que tu fais sur ce plan-là, et encore une fois je trouve les consignes confuses, donc je préfère ne pas me prononcer, mais peut-être faut-il demander à Louis ou Alan ce qu'ils en pensent ?

3. partie 4 : c'est demandé dans les consignes ce point sur ta carrière ? Ah, c'est peut-être parce que tu fais le "3ème concours" ? Mais d'après le § final des consignes, je comprends qu'ils attendent que tu valorises ton expérience uniquement quand c'est en rapport avec le projet proposé (donc Pyticoq et les idées pédagogiques qui en découlent), mais encore une fois tu dois savoir mieux que moi, et il faut voir ce qu'Alan et Louis te diront.

4. Attention dans ton README de l'archive: tu as des TODOs, des "on verra", etc. Je suppose que tu en es conscient, mais je préfère préciser.

# Remarques détaillées
- **DONE** §1(Résumé) : logique -> logiques

- **DONE** §3 : à logique naturelle les sources documentées -> à la logique naturelle, les sources documentées (et d'ailleurs, il me semble, mais je suis loin d'être un expert, qu'on parle uniquement de **"déduction naturelle"**, pas de logique naturelle, mais Louis ou Alan sauraient mieux que moi ; le terme "logique naturelle" apparaît plusieurs fois dans le document)<BR/>
**Exact, : remplacer logique naturelle par déduction naturelle**

- **DONE**fin de page 1, colonne de gauche : Form(BNF) -> Form (BNF) ; en fait **à chaque fois que tu as un sigle en version longue suivie de sa version courte entre parenthèses, il manque un espace avant la parenthèse ouvrante**<BR/>

- **DONE** fin de page 1, colonne de gauche : la syntaxe BNF de "formule" coupée en deux colonnes, ce n'est pas très élégant, mais je suppose que tu as des contraintes d'espace.<BR/>**TODO : affichage des formules**

- début de page 1, colonne de droite : "code  1" -> "code 1"
- **DONE** début de page 1, colonne de droite : (P -> Q) <-> ~ P \/ Q, il faut mettre les parenthèses autour du -> comme je l'ai fait, sinon c'est ambigu

- **DONE** début de page 1, colonne de droite : la formule "Equivalent((Implique..." est vraiment illisible, tu peux enlever une partie des parenthèses et mettre de l'indentation, par ex.
  Equivalent (Implique (Var "P", Var "Q"),
              Ou (Non (Var "P"), Var "Q"))
  ou même sur 3 lignes si tu as la place :
  Equivalent (
    Implique (Var "P", Var "Q"),
    Ou (Non (Var "P"), Var "Q"))

- **DONE** la S-expression, pareil, il faut de l'indentation (et d'ailleurs pourquoi tu utilises des guillemets différents " <> ' ?

- **DONE** début de page 1, colonne de droite : Somme est en italique les fois précédentes, mais pas là

- **DONE** § Typage : type différents -> types différents

- § Règle logique :
  - **DONE** on lit la règle -> on lit une règle / les règles, la règle, on se demande un peu laquelle

  - **DONE** attention dans le "haut en bas", tu dis que Gamma contient f1, c'est réducteur ; en toute généralité, il suffit que Gamma permette de déduire f1; d'ailleurs tu l'expliques bien dans le "bas en haut"

  - **DONE** peut-être mentionner dès le départ que ta règle, c'est le modus ponens ?

- § Preuve et but :
  - **DONE** apply h -> un peu inexpliqué, peut-être "ce qui se fait dans le système avec la tactique apply"
  - **DONE** preuve présenté -> présentée
  - **DONE** l'exemple d'exécution de Pyticoq 2 -> on dirait que tu parles du logiciel "Pyticoq 2", il faut tourner la phrase autrement
  - **DONE** a prouver -> à prouver
  - **DONE** on peut donc appliquer hyp0 -> c'est pas hyp1, plutôt ?

- § Modélisation :
  - **Done**la formule du dessous, tu peux dire qu'on l'appelle la conclusion
  - **DONE**A -> À (mais ça c'est du pinaillage)

- § Programmation récursive :
  - **DONE** buts à successifs -> buts successifs
  - **DONE** issues -> issus
  - **DONE**programme -> se programme

- § d'après:
  - **DONE** syntaxe conrète des langages -> il y a plusieurs langages ?
  - **DONE** syntaxe abstraites -> syntaxe abstraite
  - **DONE**tache -> tâche
  - **DONE**produite -> produits (et mettre une virgule)
  - **DONE** les tuples de Python sont-ils une structure de données récursives ? Je pense que c'est sujet à débat.

- § Qualité logicielle :
  - **DONE**pallient au -> pallient le (mais tout le monde fait la faute)
  - **DONE** garantie -> garantit

- § Architecture :
  - **DONE**se comprend -> comprend
  - **DONE**de pour -> pour
  - **DONE**module principale -> module principal

- partie 3
  - 1er § :
    - **DONE**assorti -> je dirais "associé" plutôt
  - § Programmation
    - permit -> permis
    - certains nombre -> certain nombre
  - § Introduction à la Logique :
    - d'entre nous -> qui ça ? Les profs ? Les Français ?
    - Comme quand... -> phrase pas claire, on comprend que tu parles de la logique, mais la structure est bizarre (mais peut-être que tu n'as pas fini de toucher à ce §)
    - les table -> les tables

- partie 4
  - **DONE**des système d'exploitation -> des systèmes d'exploitation
  - **DONE**aléatoirs -> aléatoires
  - **DONE**1ère occurrence de Celtique -> on sait pas ce que c'est. Je dirais "Au sein de l'équipe Celtique, dont les sujets ..., je faisais ...".

  - **DONE**débuggeur -> là encore je pinaille, mais ailleurs tu as utilisé bogue, peut-être utilisé débogueur ?

  - **DONE**en dans -> dans

  - § Valorisations
    - **DONE**j'ai découvert mon sujet -> je pense que ça se dit pas (d'ailleurs je ne suis pas 100% sûr du sens de ta phrase) -> peut-être reformuler en "découvert ces sujets" ? mais sens un peu différent*

    - **DONE**aux travers de -> au travers de

    - **DONE**supérieur j'ai découvert -> supérieur, j'ai découvert -> hésite pas à mettre des virgules pour séparer les propositions de tes phrases ; souvent ça facilite la lecture, parfois c'est même dur à lire sans

    - **DONE**dernier § : on comprend pas forcément le "donc" -> pourquoi la reconnaissance te donnerait envie de transmettre "les valeurs d'un scientifique et d'un citoyen intègre" ? à voir si tu peux tourner ça autrement
