Toute l'analyse et l'architecture logicielle de l'outil présentées sont issues du chapitre 16 \emph{Vérification de formules logiques}
du livre \emph{Apprentissage de la programmation avec OCaml}\cite{dubois2004apprentissage} de \emph{Catherine Dubois} et \emph{Valérie Ménissier-Morain}.
Une première version de l'outil date de 2019. Celle que nous présentons a été revue et finalisée pour le concours.

Pyticoq est un outil en ligne de commande développé en Python. C'est un assistant de preuve  
pour la logique propositionnelle inspiré de \emph{Coq}\cite{coq}. Nous décrivons dans cette partie quelques caractéristiques notables de l'implémentation et de la méthodologie de développement.
Pour plus de détails sur l'outil et une introduction sommaire à la déduction naturelle les sources documentées, fonctionnelles, et exécutables sont intégrées à ce document.

\paragraph{Types}

Deux différences majeures entre les langages OCaml et Python ont fait de l'implémentation de Pyticoq un vrai exercice d'adaptation.

On dispose en OCaml du type \emph{Somme} qui permet de définir un type agglomérant
différents constructeurs. Il est très simple avec cette fonctionnalité du langage de définir des types 
récursifs. 

\begin{figure}[!htb]
    \centering
    \makebox[0pt][c]{%
    		\begin{minipage}{0.6\linewidth}
    			\centering
    			\begin{align*}
    				&formule =\\
    				&|\ Vrai\\
    				&|\ Faux\\
    				&|\ var\\
    				&|\ \neg{formule}\\
    				&|\ formule\land{formule}\\
    				&|\ formule\lor{formule}\\
    				&|\ formule\rightarrow{formule}\\
    				&|\ formule\leftrightarrow{formule}
    			\end{align*}
        		\caption{\\Syntaxe BNF}
        		\label{fig:formuleBNF}
    		\end{minipage}%
    		%\hspace{5mm}
	    \begin{minipage}{0.6\linewidth}
	        \centering
    			\lstinputlisting[style=OCaml, basicstyle=\small]{tex/formuleBNFOCaml.tex}
        		\caption{Type OCaml}
        		\label{fig:formOcaml}
    		\end{minipage}
    		
    	}	
\end{figure} 

\subparagraph{Syntaxe abstraite}

On peut alors produire un type de données très proche de la syntaxe d'un langage dans sa représentation \emph{Backus Naur Form} (BNF). 
C'est un des atouts de OCaml comme langage pour la compilation.  Les figures ~\ref{fig:formuleBNF} et ~\ref{fig:formOcaml} présentent ce parallèle entre la
 syntaxe et le type qui la représente. 


  
Le type \emph{Somme} n'existe pas en Python. Nous avons utilisé à la place les \emph{n-upplets} comme des \emph{S-expression}. La figure ~\ref{fig:typs} présente la représentation abstraite
de la formule \( (P \rightarrow Q) \leftrightarrow \neg P \lor Q \) dans chacun des langages. Les constructeurs du type \emph{Somme} de OCaml sont remplacés par une chaîne de caractères dans le membre gauche de la paire représentant une construction grammaticale du langage.
 
\begin{figure}[!htb]
\centering
\begin{subfigure}[b]{0.4\textwidth}
	

   \begin{align*}
    				&Equivalent\\
    				&\quad (Implique\ (Var\ "P",\ Var\ "Q"),\\
    				&\quad Ou\ (Non\ (Var\ "P"),\ Var\ "Q"))
    	\end{align*}
   \caption{Type OCaml}
   \label{fig:typOC} 
\end{subfigure}

\begin{subfigure}[b]{0.4\textwidth}
\overfullrule=0pt
   \begin{align*}
    				&"Equivalent",\\
    				&\quad  (("Implique",\ (("Var",\ "P"),\ ("Var",\ "Q"))),\\
    				&\quad  ("Ou",\ (("Non",\ ("Var",\ "P")),\\
    				&\quad \quad ("Var",\ "Q"))))
   \end{align*}
   \caption{Tuple Python}
   \label{fig:typPyt}
\end{subfigure}

\caption{Représentations de la formule\\      \( (P \rightarrow Q) \leftrightarrow \neg P \lor Q \) }
\label{fig:typs}
\end{figure} 
 
\subparagraph{Typage} 
 
L'autre différence concerne les systèmes de typage de chacun des langages.
OCaml est un langage au typage fort et statique. Quand un type est donné à une
variable c'est définitif, il ne peut pas changer. Le typage est vérifié avant la compilation
ou l'interprétation. Ces deux qualités contraignent le programmeur à une certaine
rigueur lors du développement et font disparaître une large variété de bogues.

Python est un langage au typage dynamique. Une variable peut faire successivement
référence en mémoire à des objets de types différents . C'est aussi un langage interprété 
à la volée. Une erreur peut persister dans le code sans que l'on ne s'en aperçoive tant 
qu'il n'a pas été exécuté.

\subparagraph{Règle de déduction naturelle}
%%https://www.logicmatters.net/latex-for-logicians/
%https://www.logicmatters.net/resources/pdfs/latex/BussGuide2.pdf
Pyticoq applique les règles de la déduction naturelle pour résoudre des buts de preuves.
La figure  ~\ref{fig:modpo} présente le \emph{Modus Ponens}, une des règles de la déduction naturelle.

\begin{figure}[!htb]

\begin{prooftree}
	\def\fCenter{\ \vdash\ }
	\Axiom$\Gamma \fCenter f1 \rightarrow f2 \hspace{0.5cm} \Gamma \fCenter f1$
	\UnaryInf$\Gamma \fCenter f2$
\end{prooftree}
\caption{Modus Ponens}
   \label{fig:modpo}
\end{figure}


On lit :
\begin{itemize}
\item De haut en bas : Si d'un contexte $\Gamma$ d'hypothèses on peut déduire l'implication 
entre une formule \emph{f1} et une formule \emph{f2}, et que de $\Gamma$ on peut déduire \emph{f1} alors on peut déduire \emph{f2} de $\Gamma$.

\item De bas en haut : Pour prouver \emph{f2} dans $\Gamma$, il suffit de prouver que \emph{f1} implique \emph{f2} dans $\Gamma$
et \emph{f1} dans $\Gamma$.
\end{itemize}


\subparagraph{Preuve et But}

Le listing ~\ref{lst:exPyt} présente un exemple d’exécution de Pyticoq où cette 
règle est mise  en œuvre avec la tactique \emph{apply \_hyp1}.

\begin{figure}[!htb]
\centering
\begin{tabular}{c}
	\lstinputlisting[basicstyle=\small]{tex/exemple_pyticoq.tex}
\end{tabular}
\caption{Exécution de Pyticoq}
\label{lst:exPyt}
\end{figure}

Au 3ème but la formule \emph{f2} est à prouver et on a à la fois \emph{f1} et \emph{f1} $\rightarrow$ \emph{f2} (hypothèses \emph{\_hyp0} et \emph{\_hyp1}). On peut donc appliquer l'hypothèse \emph{\_hyp1} et substituer
la formule \emph{f1} à \emph{f2} grâce au \emph{modus ponens}.

\subparagraph{Modélisation}

L'ensemble des hypothèses présentes au dessus de la barre
est appelé le \emph{contexte} du but et sa conclusion est la  \emph{formule} à prouver.
Les structures de données utilisées dans l'outil pour les modéliser sont :
\begin{itemize}
\item Un dictionnaire pour le but, il a deux clés, \emph{contexte} et \emph{formule}.
\item \'A la première est associée un autre dictionnaire pour le contexte. Ses clés sont des identifiants d'hypothèses et leurs valeurs des n-upplets de syntaxe abstraite de formule logique.
\item La deuxième est associée au n-upplet de syntaxe de la formule à prouver.
\end{itemize}