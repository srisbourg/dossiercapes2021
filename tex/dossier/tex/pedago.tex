Pyticoq pourrait être utilisé en collaboration avec 
l'enseignement de la spécialité mathématiques en terminale\cite{progspemathterm}. 
L'initiation à la logique s'accompagnerait alors de sa modélisation et
de la démonstration que le raisonnement est mécanisable. Nous n'avons pas exploité cette piste.

Nous présentons dans cette partie une séquence d'enseignement et
le matériel pédagogique associé s'inspirant de certaines caractéristiques structurelles de l'outil.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subparagraph{Ressources} Pyticoq est une adaptation d'un exercice 
dans un livre d'initiation à la programmation fonctionnelle. Le style
de programmation s'en ressent tant dans la représentation des données
que dans l'élaboration des traitements. La passage de OCaml
à Python a aussi été déterminant quant à la méthode de développement adoptée.

C'est un projet \emph{dirigé par les types}, le choix pertinent des
structures représentant les syntaxes abstraites facilite par la suite le développement
des traitements. Le développement a  aussi été \emph{dirigé par les tests}.
Lors de \emph{tests unitaires} pour fiabiliser la programmation ascendante,
 et en utilisant des techniques de \emph{programmation défensive} sur les arguments des fonctions de l'outil.% pour s'assurer de la validité des arguments passés aux
 %fonctions et des valeurs de retour de ces dernières. 
%  Des tests unitaires ont aussi été effectués.
%Des fonctions de vérification de n-upplets sont présentes partout où cela était nécessaire,
%parfois avec redondance.

Le projet exploite de nombreuses ressources disponibles dans
le langage Python.
\begin{itemize}
\item Des types primitifs comme les entiers, les chaînes de caractères, les booléens, les listes, les n-upplets ou les dictionnaires
\item Des structures de contrôle classiques, les boucles ou les tests
\item Des fonctions, de la récursion, de la programmation fonctionnelle et des modules
\end{itemize}
Ces techniques font écho aux thématiques correspondantes dans les programmes des spécialités SNT et NSI au lycée.
Par leurs utilisations dans ce projet, il y a largement la matière pour introduire
les concepts fondamentaux de la programmation avec Python et les bonnes pratiques du développement.

\subparagraph{Programmation récursive} Une preuve est une liste de buts successifs à prouver. L'outil gère précisément cette liste.
Elle contient des dictionnaires représentant les buts.
L'application des tactiques est alors un traitement sur le but courant, la tête de liste.
 Ce traitement modifie le but selon les règles de la déduction naturelle. Il supprime le but 
 courant ou en ajoute de nouveaux issus de l'application de la tactique sur le but initial.
La preuve est terminée quand il n'y a plus de but à prouver, c'est à dire quand la liste est vide.
Un tel algorithme sur une liste se programme "facilement" récursivement.

Il y a deux langages dans Pyticoq, celui des formules logiques et celui des tactiques.
Une partie de l'application concerne l'analyse
lexicale et syntaxique de chaînes de caractères représentant la syntaxe concrète
des langages pour produire les n-upplets de syntaxe abstraite. La génération 
des n-upplets est effectuée à l'aide d'un algorithme d'analyse syntaxique récursif.

De nombreuses fonctions de vérification des n-upplets de syntaxe ont été
développées. Elles s'appliquent récursivement dessus.
Les fonctions des différents modules de l'outil les utilisent pour 
vérifier la cohérence de leurs paramètres . Elles pallient le typage
dynamique de Python. 

Considérant ces récurrences dans l’utilisation de la récursivité,
nous avons choisi de présenter une séquence de cours sur le sujet.

\subparagraph{Enseigner la Récursivité en terminale NSI}

Les bases de la programmation Python sont abordées en SNT durant l'année de Seconde.
C'est un enseignement qui succède aux initiations à l'algorithmie avec \emph{Scratch} en mathématiques au collège \cite{repmatc4}.
Cette découverte se poursuit en spécialité NSI en 1ère. On note que c'est seulement à ce niveau que les \emph{n-upplets} et les \emph{dictionnaires} sont introduits,
ainsi que la méthodologie d'analyse et de conception.
La récursivité est au programme de NSI de terminale. C'est le premier chapitre du livre de Balabonski et al.\cite{nsiterm}, prélude nécessaire
à l'introduction de structures de données ou de méthodes algorithmiques récursives.

Même avec la compréhension du fonctionnement de la machine et des algorithmes, quand on la voit à l’œuvre et que l'on se rend compte des services qu'elle rend, 
la récursion est une technique qui donne une impression de "\emph{magie}" de la part des 
langages et des machines. Ce qui aurait été un traitement impératif laborieux devient une solution sobre et élégante.
L'objet de cette séquence de cours est de présenter la récursivité lors d'un cours et de la travailler lors d'exercices dirigés et de travaux pratiques.\newline

Durant le cours, l'introduction s'appuiera assez naturellement sur les définitions
par récurrence des suites numériques vues en mathématiques. Un rappel sera peut-être nécessaire
si certains élèves ne  suivent ni la spécialité ni l'enseignement complémentaire de la matière.

Sur cette seule base, on peut entamer des travaux dirigés pour découvrir la programmation 
de suites numériques et de fonctions classiques opérant sur les listes \cite{mdlistoc}. Ceci 
avant de revenir au cours pour décrire le  \emph{modèle d'exécution} de la récursion
avec le support du \emph{Python Tutor}\cite{pytut}. On affinera la technique  par la pratique :
\begin{itemize}
\item Compréhension de l'algorithme et de la structure de données sur laquelle il opère
\item Définition du ou des cas de base
\item \'Elaboration du ou des  cas récursifs
\item Vérification de la convergence de la fonction vers le cas de base
\item Utilisation d'\emph{accumulateurs} ou de \emph{fioul} (figure \ref{lst:tapis}, argument \emph{profondeur})
\end{itemize}

Nous ne présentons pas le cours ou les travaux dirigés dans ce dossier faute de temps, mais
un exemple de travaux pratiques conçu pour cette séquence.

\subparagraph{Travaux Pratiques}
L'objectif du TP est de dessiner la fractale du \emph{Tapis de Sierpiński} avec le module
de \emph{Tortue Logo}\cite{mdturtle} de Python (figure ~\ref{fig:tapis}).
La version corrigée du sujet et la version élève sont insérées dans ce document.

\begin{figure}[!h]
\centering
\includegraphics[scale=0.4]{img/tortueTapis}
\caption{Tortue Logo dessinant le\\ Tapis de Sierpiński}
\label{fig:tapis}
\end{figure}

L'exercice se déroule ainsi:
\begin{enumerate}
\item Rappel sur les notions vues en cours et en exercices dirigés et exemple de fonction récursive pour une suite numérique
\item Prise en main du module \emph{turtle} de Python
\item Implémenter la fonction \emph{carre(x, y, longueur)} qui trace un carré à partir de son sommet \emph{sud-est} (figure ~\ref{fig:nomsom}).
\item \label{8fct} Implémenter les 8 fonctions des carrés \emph{enfants} (figure ~\ref{fig:nomenf}) telles que \emph{enfant\_est(x, y, longueur)} qui prend en paramètre:
	\begin{itemize}
	\item \emph{x}, l'abscisse du sommet \emph{sud-est} du carré \emph{parent}
	\item \emph{y}, l'ordonnée du sommet \emph{sud-est} du carré \emph{parent}
	\item \emph{longueur}, la longueur du carré \emph{parent}
	\end{itemize}
	Elle retourne un triplet (\emph{x'}, \emph{y'}, \emph{longueur'}) les coordonnées du sommet \emph{sud-est} 	    du carré enfant \emph{est} et sa longueur de côté.
\item Implémenter, en utilisant les 8 fonctions définies précédemment, la fonction \emph{carres\_enfant(x, y, longueur)} qui renvoie la liste des 8 triplets
des carrés \emph{enfants} à partir  des coordonnées \emph{x} et  \emph{y} et \emph{longueur} de côté d'un carré parent.
\item \label{fntapis} Implémenter la fonction\\ \emph{tapis (liste\_coords\_taille, profondeur)} qui prend en paramètre une 
liste de triplets et une profondeur et qui renvoie une liste de triplets (figure  \ref{lst:tapis}).
\end{enumerate}


\begin{figure}[!htb]
\centering
\begin{subfigure}[b]{0.4\textwidth}
	\centering

   \includegraphics[scale=0.4]{img/noms_enfants}
	\caption{Carrés enfants}
	\label{fig:nomenf}
\end{subfigure}

\begin{subfigure}[b]{0.4\textwidth}
\overfullrule=0pt
\centering
   \includegraphics[scale=0.4]{img/noms_sommets}
	\caption{Sommets}
	\label{fig:nomsom}
\end{subfigure}

 	\caption{Nomenclature des carrés enfants et des sommets}

\end{figure} 

Il y a plusieurs intérêts à effectuer ce travail en utilisant la Tortue Logo:
\begin{itemize}
\item On visualise graphiquement et en direct le déroulement de l'algorithme.
\item L'élève intègre l'algorithme et ses considérations spatiales lors de l'étape ~\ref{8fct}.
\item \'A l'étape ~\ref{fntapis}, finir par trouver le bon appel récursif (figure ~\ref{lst:tapis}) n'est pas évident. L'élève va pouvoir expérimenter et visualiser ses erreurs pour avancer vers la solution.
\end{itemize}

\begin{figure}[!h]
\centering
\overfullrule=0pt
\lstinputlisting[style=Python,  basicstyle=\small]{src/tapis.py}
\caption{Fonction tapis}
\label{lst:tapis}
\end{figure}

Le matériel est fourni à l'élève sous forme d'un document qui contient le fichier
\emph{Jupyter} sur lequel travailler. Il dispose ainsi d'un document unique non modifiable
et des sources pour travailler. Cette méthodologie de travail, utilisée pour la rédaction
du présent dossier, sensibilise aussi à la communication scientifique et à l'informatique
comme \emph{science reproductible}.











































