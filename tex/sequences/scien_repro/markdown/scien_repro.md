Communication comme un Code
===

# Introduction à Markdown

## Installation de l'éditeur
Installer [Visual Studio Code](https://code.visualstudio.com/#alt-downloads)

## Préparation du répertoire de travail
1. Créer le répertoire ```0_Intro_Markdown``` dans
votre espace de travail
2. Lancer l'éditeur

## Créer un fichier
+ Par le menu : ```Fichier``` > ```Nouveau Fichier```

+ Par le raccourci : ```touche Ctrl``` + ``` touche N```

## Enregistrer le fichier
+ Par le menu ```Fichier``` >  ```Enregistrer sous...``` >
  Aller dans le répertoire ```0_Intro_Markdown``` >
  Choisir ```comcomuncode.md``` comme nom de fichier >
  Cliquer sur ```Enregistrer```

+ Par le raccourci : ```touche Ctrl``` + ``` touche S``` >
  Aller dans le répertoire ```0_Intro_Markdown``` >
  Choisir ```comcomuncode.md``` comme nom de fichier >
  Cliquer sur ```Enregistrer```

## Prévisualiation de la page
* Ouvrir la fenêtre de prévisualisation : (```touche Ctrl``` + ``` touche S```) + ```touche V``` (D'abord ```Ctrl+K``` en même temps, puis ```V```)
+ Un nouveau panneau latéral s'affiche dans la
partie droite de l'éditeur.

## Titre
1. Ligne 1 : écrire le titre ```Communication comme un Code``` 
2. Ligne 2 : souligner ainsi : ```===```
3. Le titre s'affiche dans le panneau latéral :
![titre](img/0_titre.png)

## Section
1. Ligne 3 : Sauter 
2. Ligne 4 : 
+ Commencez la par ```#```
+ Suivi du titre de la section : ```Introduction à Markdown```
3. La section doit s'afficher ainsi :
![section](img/1_section.png)

## Sous Section
1. Ligne 5 :
+ Commencez la par ```##```
+ Suivi du titre de la sous section : ```Installation de l'éditeur```

2. La sous section s'affiche ainsi : 
![sous_section](img/2_sous_section.png)

3. **REMARQUE:**
+ Pour faire des Sous-Sous-Section préfixer par ```###```
+ Pour faire des Sous-Sous-Sous-Section préfixer par ```####```
+ Et cetera ...

## Création d'un lien
Pour ajouter un lien vers une ressource sur le web, la syntaxe est la suivante:
1. Ligne 7 : 
* Ecrire le verbe ```Installer``` normalement
* Suivi du texte du lien ```"Visual Studio Code"``` **entre deux crochets**. C'est le texte qui va s'afficher dans le panneau latéral.
* Suivi de l'adresse ```https://code.visualstudio.com/#alt-downloads``` de la ressource **entre parenthèses**. 

2. Le lien s'affiche ainsi dans le panneau latéral :
![lien](img/6_lien.png
)

## **Exercice** : Créer la deuxième sous section
* Le titre de la deuxième sous section s'affiche ainsi :
![section2](img/4_section2.png)
* Créez le

## Liste énumérée
* Dans une liste énumérée chaque ligne commence par ```1.```,```2.```, ```3.```, ```4.```, ...

* La premier élément s'affiche ainsi :
![listenum](img/5_listenum.png)

* **Exercice** : Créer le

## Souligner du texte
* Dans l'image précédente on constate que le texte ```0_Intro_Markdown``` est en jaune.

* Pour cela on encadre le texte avec 3 accents grave (touche du 7) : \`\`\`0_Intro_Markdown\`\`\`

* Modifier la ligne pour surligner le texte dans le panneau latéral

## Terminer la deuxième sous section
Le deuxième élément de la liste s'affiche ainsi :
![element2](img/7_listenum.png)

## Liste simple