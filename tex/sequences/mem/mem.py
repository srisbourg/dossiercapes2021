"""
    Module mémoire comme une bataille navale
"""

def int_matrix(n, v=0):
    """
        * int_matrix retourne une matrice carrée d'entier de taille n  initialisée à v
        * v est un paramètre optionnel qui vaut 0 par défaut
        * retourne None si n n'est pas un entier ou s'il est inférieur à 0
    """
    if type(n) != int or n < 0:
        return None
    return [[v for i in range(n)] for i in range(n)]

def str_line(l, sep='|', length=None):
    """
        * str_line écrit un le tableau l 
    """
    if l == []:
        return sep
    else :
        h, *t = l
        if not length:
            return sep + str(h) + str_line(t, sep, length)
        else:
            strspace = ' ' * ((length - len(str(h))) // 2)
            return sep + strspace + str(h) + strspace + str_line(t, sep, length)

def str_matrix(m, sep='|', length=None):
    if m == []:
        return ''
    else:
        h, *t = m
        return str_line(h, sep, length) + '\n' + str_matrix(t, sep, length)

def str_line_square(length, hidesep=False, hidebar=False, val = None, sep='|', bar='_', space=' '):
    if hidesep:
        return space + bar * length
    if hidebar:        
        if val!= None:
            s = str(val)
            strspace = space * ((length - len(s)) // 2)
            if len(s)%2 == 0:
                return sep + strspace + s + strspace
            else:
                return sep + strspace + s + strspace + space
        else:
            return sep + space * length
        
    return sep + bar * length

def str_square_matrix(m, length=10, exceptlist=[]):
    if m == []:
        return ''
    else:
        size = len(m)
        
        # Top square line
        ret = str_line_square(10, True) * size + '\n'
        
        for i in range(size):
            
            # First square line
            for j in range(size):
                ret += str_line_square(10, False, True)
            ret += '|\n' 
            
            # Middle square line
            for j in range(size):
                v = m[i][j]
                if v in exceptlist:
                    ret += str_line_square(10, False, True)
                else :
                    ret += str_line_square(10, False, True, v)
            ret += '|\n'
            
            # Last square line
            for j in range(size):
                ret += str_line_square(10, False)
            ret += '|\n'
        return ret