# Dossier Capes 2021

Dossier pour la Seconde épreuve d'admission (épreuve sur dossier)
[https://capes-nsi.org/](https://capes-nsi.org/)

* [Dossier](tex/dossier/release/dossiercapes2021SamuelRisbourg_v1.1.pdf)
* [Présentation](tex/presentation/release/presentationCapes2021SamuelRisbourg_v1.1.pdf)
* [Pyticoq](https://gitlab.com/srisbourg/pyticoq)
* [TP récursion](tex/sequences/recurs/tp_recurs/tp_recurs.pdf)
* [Rapport du jury du 3ème concours du CAPES-CAFEP NSI 2021](rapport_jury_3econcours_2021.pdf)

# Seconde épreuve d'admission (épreuve sur dossier)

Durée de l'épreuve : 1 heure (30 minutes de présentation suivies de 30 minutes d'entretien)
Coefficient 2

* L'épreuve repose sur une présentation d'un dossier réalisé par le candidat, suivie d'un entretien avec le jury. 

* **L'épreuve doit permettre d'apprécier la capacité du candidat à présenter de manière réfléchie un ou plusieurs projets ou réalisations informatiques, et à en extraire des exploitations pédagogiques pertinentes pour son enseignement au lycée**.

* **L'épreuve permet d'apprécier la capacité du candidat à mobiliser les notions et concepts des programmes** en mettant en œuvre une pédagogie de projet et en se montrant attentif aux dimensions éthiques, juridiques, économiques ou environnementales des problèmes abordés.

* L'entretien permet au jury d'approfondir les points qu'il juge utiles. Il permet aussi d'**évaluer la capacité du candidat à prendre en compte les acquis et besoins des élèves, à se représenter la diversité des conditions d'exercice de son métier futur, à en connaître de façon réfléchie le contexte dans ses différentes dimensions (classe, équipe éducative, établissement, institution scolaire, société) et les valeurs qui le portent, dont celles de la République**. 

# Épreuve sur dossier

## Dossier
* Le candidat devra préparer, individuellement, un dossier sur un sujet de son choix en lien avec le programme du Capes NSI. 

### Activités pédagogiques
* Dans ce dossier, le candidat devra proposer et développer une ou plusieurs activités pédagogiques, comme par exemple, une séance d'enseignement (un cours, un TD ou un TP) et/ou un projet pour les élèves, en lien avec le thème choisi.

* Les activités pédagogiques proposées peuvent concerner différents niveaux de classe. Ce dossier, ainsi que l'oral, doit permettre au jury d'évaluer les capacités du candidat à exploiter, pour son enseignement, ses connaissances et/ou différentes ressources de son choix.

### Ressources
* Les ressources utilisées pourront être des ressources existantes ou un ou des projets réalisés par le candidat. Les sources des ressources existantes devront être citées.

* Le jury attend des développements personnels approfondis de nature disciplinaire conformes aux exigences du concours et faisant référence aux exploitations pédagogiques possibles. **Une compilation de ressources existantes non complétée d'une étude personnelle et d'exploitations pédagogiques pertinentes sera insuffisante.**

* Les ressources exploitées par le candidat pourront être : des extraits de manuels scolaires, des extraits d'articles scientifiques de recherche ou pour le grand public, des extraits d'articles de presse, du code informatique (déjà existant ou réalisé par le candidat), de la documentation de code, de bibliothèque ou de logiciel, des schémas d'expérimentation, etc. 

* **Le dossier, comprenant entre 4 et 6 pages (format A4), décrira de manière synthétique le thème retenu par le candidat et les différentes activités proposées selon les niveaux de classe considérés**.

* **Le candidat pourra aussi indiquer comment les ressources seront utilisées avec les élèves et dans quels buts, quelles sont les connaissances et compétences visées, etc.**

### Soumission
* Le dossier devra être soumis, au format pdf, au moins 10 jours avant le début des oraux. Le site de soumission sera précisé ultérieurement.

### Oral
#### Déroulement
* Lors de l'oral, le candidat devra défendre son dossier lors de 30 minutes de présentation.

* Il devra motiver et expliquer les ou une partie des activités proposées dans le dossier.

* Toutes les activités proposées dans le dossier ne devront pas nécessairement être présentées à l'oral. C'est au candidat de faire ses choix.

* La présentation sera suivie de 30 minutes d'entretien pendant lesquelles le jury approfondira tous les points qu'il juge utiles.

#### Annexes

* Le candidat aura la possibilité de soumettre tout document qu'il juge utile pour l'oral (transparents de présentation, code, documentation, etc.) au sein d'un ficher zip ne dépassant pas la taille de 20 Mo. Ce fichier devra être soumis au moins 3 jours avant le début des oraux. Le site de soumission sera précisé ultérieurement. 

* Le candidat pourra aussi, s'il le souhaite, apporter tout matériel sans électronique qui pourra être utilisé lors de la présentation. Ce matériel ne doit comporter aucune matière ou matériel dangereux. Le jury se réserve le droit de refuser l'utilisation de ce matériel à l'oral. **Il est à noter que le jury évaluera surtout le fond de la présentation.**

#### 3ème concours
* Concernant le troisième concours, le candidat devra, en plus des recommandations précédentes, **valoriser les acquis de son expérience dans le problématique du sujet qu'il aura choisi de traiter**.
