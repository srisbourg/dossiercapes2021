Pyticoq - Vérification de formules logiques
===

![Pitikok](img/pitikok.jpg)<BR/>
[Pyticoq découvre Python à ses riques et périls, vont-ils devenir amis ? Tous droits réservés aux P'tites Poules](https://www.lisez.com/index.php?tipo=series&nombre=les-ptites-poules&ser=54501&pagina=catalogo&sello=pocket-jeunesse)

# Présentation
* Ce projet est une adaptation en python du TP du chapitre 16 [**Vérification de formules logiques**](https://www-pequan.lip6.fr/~vmm/Livre_prog_CD_VMM/HTMLSRC/assistant.ml.html)
du livre <BR/> [**Apprentissage de la programmation en OCaml de C. Dubois et al (2004)**](https://www-pequan.lip6.fr/~vmm/Livre_prog_CD_VMM/)

* L'exemple d'implémentation des auteurs est exécutable avec la commande ```make ocamlproj``` (inutilisable pour l'instant)

* **Pyticoq** est un mini assistant de preuve de la logique propositionnelle en ligne de commande, inspiré
de l'assistant de preuve [**Coq**](https://coq.vercel.app/). 

* C'est un outil qui pourrait être utilisé pour l'initiation à la démonstration ou à l'étude de la correspondance entre
une spécification et une implémentation.

# Dépendances

## Système
* Debian 10.5
* make
* sed 
* pandoc
* htmldoc

## Python
* Python 3.9
* Jupyter Notebook
* ipython
* ipynb
* pylint

## OCaml
* opam
* ocaml 4.12.0

## Linux
## Compilation
Exécuter  la commande ```make```

## Tests
Exécuter  les commandes ```make tests```, ```make tests_pyticoq```

## Windows
Dans le répertoire de de l'outil exécuter la commande ```python lib\pyticoq``` 

# Utilisation
1. Le point d'entrée du l'outil est le fichier ```lib\pyticoq```
2. On peut l'appeler de 3 manières :
* Sans paramètre<BR/>
  C'est ce que fait la commande ```make run``` avec la commande ```python3.9 lib/pyticoq```<BR/>
  L'utilisateur doit alors saisir une formule logique à prouver.<BR/><BR/>
* Avec le paramètre ```--formule```<BR/>
  Pour donner directement la formule sous forme de chaine de caractères au programme <BR/>et lancer 
  directectement l'assistante de preuve. Un exemple de de cette utlisation est être<BR/>
  ```python3.9 lib/pyticoq.py --formule "(A/\B==>Q)==>A==>B==>Q"```<BR/><BR/>
* Avec les paramètres ```--formule``` et ```--tactics```<BR/>
  Pour associer à la formule à prouver les tactiques à appliquer, le paramètres est aussi une chaîne<BR>
  de caractères de tactiques séparées par des points virgules ```;```<BR/>
  La commande de tests ```make tests_pyticoq``` utilise cette méthode.<BR/>
  Voici un exemple d'utilisation : ```python3.9 lib/pyticoq.py --formule "(P==>Q)==>(Q==>R)==>P==>R" --tactics "intros h1, h2, h3; apply h2; apply h1; exact h3"```

# Déduction naturelle

Les **tactiques** décritent par la suite utilisent les **règles** ci-dessous pour 
prouver les buts des démonstration de formule.

## Symboles
* ~ pour la négation (Non)
* /\ pour la conjonction (Et)
* \/ pour la disjonction (Ou)
* ==> pour l'implication (Si ... Alors ...)
* <==> pour la l'équivalence (... Si et Seulement Si ...)
* &Delta; (Delta) pour désigner un contexte d'hypothèses
* |- pour représenter la déduction
* &euro; appartient<BR/><BR/>

## Règles

### Axiome
&Delta; |- f si f &euro; &Delta;<BR/><BR/>
D'un contexte &Delta; contenant l'hypothèse f on peut déduire f.<BR/><BR/>

### Faux
&Delta; |- Faux<BR/>
<HR/>
&Delta; |- f<BR/><BR/>
Si l'on peut déduire Faux d'un contexte, on peut en déduire n'importe quelle formule f.<BR/><BR/>

### ==> introduction
&Delta; , f1 |- f2<BR/>
<HR/>
&Delta; |- f1 ==> f2<BR/><BR/>
Pour prouver l'implication f1 ==> f2 dans le contexte &Delta;, 
il faut prouver f2 en ajoutant f1 à &Delta;.<BR/><BR/>

### [Modus ponens](https://fr.wikipedia.org/wiki/Modus_ponens) (==> élimination)
&Delta; |- f1 ==> f2, &Delta; |- f1<BR/>
<HR/>
&Delta; |- f2<BR/><BR/>
Si on peut déduire |- f1 ==> f2 de &Delta;  et |- f1 de  &Delta; alors on peut déduire f1 de  &Delta;.<BR/><BR/>

### /\ introduction
&Delta; |- f1 , &Delta; |- f2<BR/>
<HR/>
&Delta; |- f1 /\ f2<BR/><BR/>
Pour prouver f1 /\ f1 dans le contexte &Delta;
il faut prouver f1 dans &Delta; et f2 dans &Delta;<BR/><BR/>

### /\ élimination gauche
&Delta; |- f1 /\ f2<BR/>
<HR/>
&Delta; |- f2<BR/><BR/>
Si on peur déduire f1 /\ f2 de &Delta; alors on peut déduire 
f1 de &Delta;.<BR/><BR/>

### /\ élimination droite
&Delta; |- f1 /\ f2<BR/>
<HR/>
&Delta; |- f2<BR/><BR/>
Si on peur déduire f1 /\ f2 de &Delta; alors on peut déduire 
f2 de &Delta;.<BR/><BR/>


### \/ introduction gauche
&Delta; |- f1<BR/>
<HR/>
&Delta; |- f1 \/ f2<BR/><BR/>
Pour prouver f1 \/ f2 dans &Delta; il suffit de prouver  f1 dans &Delta;.<BR/><BR/>

### \/ introduction droite
&Delta; |- f2<BR/>
<HR/>
&Delta; |- f1 \/ f2<BR/><BR/>
Pour prouver f1 \/ f2 dans &Delta; il suffit de prouver  f2 dans &Delta;.<BR/><BR/>

### \/ élimination
&Delta; |- f1 \/ f2,  &Delta;,f1 |- f, &Delta;,f2 |- f<BR/>
<HR/>
&Delta; |- f<BR/><BR/>
Pour prouver f dans &Delta; il faut prouver :

* f1 \/ f2 dans 
&Delta;

* f dans &Delta; augmenté de f1

* f dans &Delta; augmenté de f2<BR/><BR/>

### ~ introduction
&Delta;, f |- f1, &Delta;, f |- ~f1<BR/>
<HR/>
&Delta; |- ~f<BR/><BR/>
Si &Delta; augmenté de  f permet de prouver à la fois f et
~f (contradiction) alors on déduit ~f de &Delta;<BR/><BR/>



# But, Contexte, Formule
Une démonstration se décompose en une séquence de **buts** sucessifs à prouver.<BR/>
Ils sont constitués d'un **contexte** et d'une **formule**. Ils se présentent sous la forme suivante dans l'outil:
```
_hyp0 : A
_hyp1 : ~A
h : A /\ ~A
==================================================
False
```
* Le **contexte** est la partie présente au dessus de la ligne. C'est une liste de formules logiques nommées.
  Dans le contexte présenté ci-dessus, on associe par exemple au nom ```h``` la formule ```A /\ ~A```.
* La **formule** du but est la partie présente sous la ligne, ici ```False```.

# Exemple de démonstration
```
make run
python3.9 lib/pyticoq.py
Saisie Formule ? A /\ ~A ==> False
Formule à prouver : A /\ ~A ==> False

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


==================================================
A /\ ~A ==> False

Saisie tactique : intro h

h : A /\ ~A
==================================================
False

Saisie tactique : decompose h

_hyp0 : A
_hyp1 : ~A
h : A /\ ~A
==================================================
False

Saisie tactique : absurd A

_hyp0 : A
_hyp1 : ~A
h : A /\ ~A
==================================================
A

Saisie tactique : exact _hyp0

_hyp0 : A
_hyp1 : ~A
h : A /\ ~A
==================================================
~A

Saisie tactique : exact _hyp1

Qed
```

# Syntaxes
**Pyticoq** est composé de deux langages distincts.

## Langage de Formules Logiques

Le langage de formule logique est codé en [```ASCII```](https://fr.wikipedia.org/wiki/American_Standard_Code_for_Information_Interchange#Table_des_128_caract%C3%A8res_ASCII), il comprend les symboles:

* ```Vrai``` pour la valeur de vérité Vrai
* Faux pour la valeur de vérité Faux
* /\   pour la conjonction (... Et ...)
* \/   pour la disjonction (... Ou ...)
* ~    pour la négation    (... Non ...)
* ==>  pour l'implication  (Si ... Alors ... )
* ```<==>``` pour l'équivalence  (... Si et seulement Si ...)
* ```(``` et ```)``` pour la parenthésage des sous formules
* Auxquels s'ajoutent des identifiant pour pouvoir écrire des formules telles que ```IlPleut ==> PrendreParapluie```


## Langage de Tactiques
Il est possible d'appliquer différentes tactiques pour résoudre un but:

### exact
```exact H``` essaye de résoudre le but courant en indiquant que sa formule à démontrer est exactement la formule ```H``` du contexte.

### split
```split``` s'applique sans paramètre uiquement sur la formule d'un but d'une des formes suivantes :

* ```f1 /\ f2```
* ```f1 <==> f2```

Elle remplace le but courant par deux nouveaux buts ayant le même contexte et respectivant f1 et f2 comme formule.

### left
```left``` s'applique uniquement sur une formule de la forme ```f1 \/ f2```.  Elle remplace le but courant par un nouveau but ayant le même
contexte et f1 pour formule.

### right
```right``` s'applique uniquement sur une formule de la forme ```f1 \/ f2```.  Elle remplace le but courant par un nouveau but ayant le même
contexte et f2 pour formule.

### intro
```intro H``` s'applique uniquement sur une formule de la forme ```f1 ==> f2```. ```H``` ne doit pas être un nom d'hypothèse présent dans le contexte. Elle remplace le but courant par un but ayant f2 pour formule et un contexte composé du contexte initial augmenté de l'hypothèse ```H : f1```.

### intros
```intros H1, H2, ... , Hn``` s'applique uniquement sur une formule de la forme ```f1 ==> f2 ==> ... ==> fn ==> f```.
Chacun des noms ```H1```, ```H2```, ..., ```Hn``` doivent être nouveaux (absents du contexte initial). Elle remplace le 
but courant par un but ayant f pour formule et un contexte composé de du contexte initial augmenté des hypothèses ```H1 : f1```,
```H2 : f2```, ..., ```Hn : fn```.

### apply
```apply H``` s'applique uniquement sur un but composé d'une formule f et possédant une une hypothèse  ```H``` de la forme ```H : f1 ==> f2 ==> ... ==> fn ==> f```. Elle remplace le but courant par ```n``` buts ayant le même contexte respectivement f1, f2, ..., ```fn``` 
pour formule.

### decompose
```decompose H``` s'applique uniquement sur la formule d'une hypothèse ```H``` du contexte de la forme ```H : f1 /\ f2```. 
Un nouveau remplace le  but courant, sa formule est la même et son contexte est composé du contexte initial augmenté
de deux nouvelles hypothèses auxquelles sont associèes les formules f1 et f2.# Règles de déduction de la logique naturelle#git checkout README.md
Plus tard
est alors remplacé 

### case
```case H``` s'applique sur la formule d'une hypothèse ```H``` du contexte de la forme ```H : f1 \/ f2```.
La tactique produit deux nouveaux buts ayant la même formule que le but initial et dont les contextes sont composés du contexte de départ
augmenté de part et d'autre par de nouvelle hypothèses associées chacune aux formules f1 et f2.

### absurd
```absurd f``` permet démontrer que le contexte est incohérent et permet de démontrer à la fois f et ```~f```. f est
une formule quelconque saisie par l'utilisateur. Le but initial est alors remplacé par deux nouveaux buts ayant le
même contexte et cherchant à prouver respectivement f et ```~f```.

# Idées de séquence d'enseignement (à creuser)

## Logique
* Introduction à la logique
* Tables de vérités
* Déduction naturelle

## Progammation
* Paradigmes de programmation
* Programmation dirigée par les tests
* Programmation dirigée par les types

## Langages
* Typage faible/fort, statique/dynamique
* Initiation à la programmation
* Représentation
* Syntaxe abstraite et concrête
* Interprétation
* Markdown
* Python
* Shell
* OCaml

## Méthodologie et pensée informatique
* Compilation
* Automatisation

## Initiation à la récursion
1. Communication comme un code avec Latex et Texmaker
2. Makefile, compilation et automatisation
3. L'informatique comme science reproductible et intuition de la récursion

## Système
* Linux
* Shell

# Remarques
* Le code est traduit en markdown dans le répertoire [md](md) pour faciliter la lecture sur le dépot.
* Le code est traduit en html dans le répertoire **html** pour faciliter la lecture hors ligne.

# Documentation
* **TODO** : Utilisation ...
* Les fonctions sont (presque toutes) commentées

# Licence
On verra
