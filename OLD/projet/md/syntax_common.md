```python
def ignore_char(c, l =[' ', '\n', '\t']):
    """
    * La fonction ignore_char retourne True si le caractère c
    appartient à la list l et Fasle sinon.
    * Par défaut l = [' ', '\\n', '\\t']
    """
    if c in l :
        return True
    return False
```


```python
def debut_mot(s, n, pos):
    """
    La fonction debut mot retourne pour une chaîne s de
    longueur n et pour une position de départ pos,
    la position du premier caractère significatif
    (que l'on ignore pas).
    """
    assert n == len(s)
    i = pos
    while i < n and ignore_char(s[i]):
        i += 1
    return i
```


```python
def fin_chaine(s, n, pos):
    """
    * La fonction fin_chaine teste dans la chaîne s de longueur n
    si les caractères suivants le caractère à la position pos
    sont non significatifs.
    * Exemple : Pour s = "jdfs    " les caractères après le s sont
    non significatifs dans notre contexte.
    """
    assert n == len(s)
    return debut_mot(s, n, pos) == n
```


```python
class SyntaxFormuleError(Exception):
    """
    Exception SyntaxFormuleError pour les fonctions d'analyse syntaxique deformule
    """
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)
```


```python
def is_keyword(keyword, s, n, pos, e=SyntaxFormuleError):
    """
    * La fonction is_keyword reconnait une un mot clé du langage comme
      une sous-chaîne keyword dela chaîne s de longeur n à la position pos. 
    * Elle retourne un triplet :
      - la chaîne s
      - sa longueur n
      - la position du mot suivant
    """
    assert len(s) == n
    kw_length = len(keyword)
    if s[pos:pos+kw_length] == keyword:
        return debut_mot (s, n, pos+kw_length)
    raise e(("function is_keyword", keyword, s, n, pos))    
```


```python
def possible(c):
    """
    * La fonction possible teste si le caractère c est :
      - soit un caractère alphètique
      - soit un caractère numérique
      - soit underscore '_'
    * Elle renvoie True si c'est le cas et False sinon
    """
    return c.isalpha() or c.isdigit() or c == '_' 
```


```python
def is_ident(s, n, pos, e=SyntaxFormuleError):
    """
    * La fonction is_ident retourne une paire :
      - un identificateur présent dans la chaîne s de taille n
        commençant à la position pos.
      - la position du prochain mot dans s
    * L'identificateur est une sous-chaîne de s
    * Si la sous-chaîne contient un caractère interdit ou que
    la paramètre pos est inadéquat la fonction lève une exception
    SyntaxError.
    """
    assert n == len(s) 
    if pos == n or not possible(s[pos]):
        raise e(("function is_ident", (s, n, pos)))
    j = 1
    while pos + j < n and possible(s[pos+j]):
        j += 1
    len_word=j
    while pos + j < n and ignore_char(s[pos+j]):
        j += 1
    return s[pos:pos+len_word], pos + j
```
