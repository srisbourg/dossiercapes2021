# 9. Analyse syntaxique d'une tactique
**Apprentissage de la programmation en OCaml de C. Dubois et al (2004)**, §16.5.5, p357

Syntaxe concrête du langage de tactiques


```python
"""
Module pour l'analyse syntaxique du langage de tactiques
"""

from ipynb.fs.full.syntax_common import debut_mot, fin_chaine, SyntaxFormuleError, is_keyword, is_ident

from ipynb.fs.full.syntax_formule import SyntaxFormuleError, is_FORM0, analyse_prop
```


```python
class SyntaxTacticError(Exception):
    """
    Exception pour les erreurs de syntaxe du langage de tactiques
    """
    def __init__(self, value):
        Exception.__init__(self)
        self.value = value
    def __str__(self):
        return repr(self.value)
```


```python
def is_COMA(s, n, pos):
    """
    La fonction is_COMA reconnait une virgule ','
    """
    return(is_keyword(',', s, n, pos, SyntaxTacticError))
```


```python


def is_HYP_aux(s,n,pos):
    """
    * La fonction is_HYP_aux détecte un identiant d'hypothèse
    * Si cette identifiant commence par un underscore '_' une exception
      SyntaxTacticError est levée
    * Sinon la fonction l'identifiant et la position du mot suivant
    """
    idt, pos1 = is_ident(s, n, pos, SyntaxTacticError)
    if idt[0] == '_':
        raise SyntaxTacticError(('is_HYP_aux', s,n,pos))
    return idt, pos1
```


```python
def is_HYP(s,n,pos):
    """
    * La fonction is_HYP reconnait des identifiants d'hypothèses séparées par des virgules.
    * Si l'hypothèse h est unique la fonction retourne une liste contenant pour unique élément h
    * Sinon elle construit récursivement la liste de toutes les hypothèses et la retourne.
    * TODO améliorer et expliquer la gestion des exceptions
    """
    h, pos1 = is_HYP_aux(s, n, pos)
    try:
        pos2 = is_COMA(s,n,pos1)
    except SyntaxTacticError as err:
        return [h], pos1
    harr, pos3 = is_HYP(s,n,pos2)
    return [h] + harr, pos3
    
```


```python
class EndException(Exception):
    """
    Exception EndException pour interrompre le prouveur interactif
    """
    def __init__(self, value):
        Exception.__init__(self)
        self.value = value
    def __str__(self):
        return repr(self.value)


def is_TACT(s,n,pos):
    """
    * La fonction is_TACT reconnait les mots clé du langage de tactiques
      dans une chaine de caractères s et construit le tuple de syntaxe abstraite
      correspondant.
    * La tactique quit lance une exception EndException
    * Si l'analyse syntaxtique détecte un mot clé non autorisé une
      exception SyntaxTacticError est levée
    """
    idt, pos1 = is_ident(s, n, pos, SyntaxTacticError)
    
    if idt in ('split', 'left', 'right'):
          if fin_chaine(s,n,pos1):
              return ((idt, None), pos1)

    if idt == 'intro':
        h, pos2 = is_HYP_aux(s,n,pos1)
        return ((idt, h), pos2)
    
    if idt in ('exact', 'case', 'decompose', 'apply'):
        h, pos2 = is_ident(s, n, pos1, SyntaxTacticError)
        return ((idt, h), pos2)
    
    if idt == 'intros':
        h, pos2 = is_HYP(s, n, pos1)
        return ((idt, h), pos2)
    
    if idt == 'absurd':
        try:
            f, pos2 = is_FORM0(s, n, pos1)
            return ((idt, f), pos2)
        except SyntaxFormuleError as err:
            raise SyntaxTacticError(('is_TACT', s,n,pos))
        
    if idt == 'quit':
        raise EndException("quit")
        
    raise SyntaxTacticError(('is_TACT', s,n,pos))
```


```python
def analyse_tact(s):
    """
    * La fonction analyse_tact tente de reconnaitre une tactique dans la chaine
      de caractères s:
      - Si la chaine est bien formée, la fonction retourne un tuple du langage de tactique
      - Sinon elle lève une exception SyntaxTacticError
    """
    n = len(s)
    t, pos = is_TACT(s,n,debut_mot(s,n,0))
    if fin_chaine(s,n,pos):
        return t
    raise SyntaxTacticError(('analyse_tact', s))
```


```python

```
