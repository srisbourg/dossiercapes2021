# 8. PytiCoq


```python
"""
Fonctions du prouveur interactif
"""

from ipynb.fs.full.syntax_formule import analyse_prop
from ipynb.fs.full.type_formule import typecheck_formule
from ipynb.fs.full.type_context import typecheck_context, valid_ident
from ipynb.fs.full.type_goal import typecheck_goal, mk_goal
from ipynb.fs.full.type_tactic import typecheck_tactic, TypeCheckTacticError
from ipynb.fs.full.syntax_tactic import analyse_tact, EndException, SyntaxTacticError
from ipynb.fs.full.printer import string_of_formule, fresh_ident, string_of_tactic, string_of_goal
import argparse
```


```python
class IdentInvalidError(Exception):
    """
    Exception IdentInvalidError
    """
    def __init__(self, value):
        self.value = value
        Exception.__init__(self)
    def __str__(self):
        return repr(self.value)

class TacticError(Exception):
    """
    Exception TacticError
    """
    def __init__(self, value):
        self.value = value
        Exception.__init__(self)
    def __str__(self):
        return repr(self.value)
```


```python
def introduction (hyps, context, formule):
    """
    * La fonction introduction prend en paramètre :
      - La liste hyps des noms des hypothèses que l'on va ajouter au contexte
      - Le contexte context
      - La formule 'formule' à prouver
    * Si la formule est une implication de la forme :
      f0 ==> f1 ==> f2 ==> ... ==> fn ==> f'
    * Si les identifiants contenuent dans hyps sont valides et de la forme :
      h0, h1, h2, ..., hm (m <= n)
    * Alors la fonction retourne une paire composée
      - D'un nouveau contexte tel que :
        new_context = {h0:f0, h1:f1, h2:f2, ..., hm:fm} | context 
      - D'une formule tel que :
        fm+1 ==> ... ==> fn ==> f'
    * La fonction s'applique récursivement sur la liste des hypothèses
    * Si le contexte contient déjà une hypothèse dont l'identifiant
      est le même qu'un de ceux contenus dans hyps une exception
      IdentInvalidError est lévée
    * Si la formule n'est pas une implication une exception TacticError
      est levée.
    """
    typecheck_formule(formule)
    typecheck_context(context)

    if hyps == []:
        return context, formule

    tag, val = formule

    if tag == 'Implique':
        head, *tail = hyps
        if valid_ident(head, context):
            f1, f2 = val
            new_context = context.copy()
            new_context[head] = f1
            return introduction(tail, new_context, f2)
        else:
            raise IdentInvalidError((head, hyps, context, formule))

    raise TacticError(('function introduction', hyps, context, formule))    
```


```python
def conditions_apply(phi, formule):
    """
    * La fonction conditions_apply prend en paramètre une formule
      phi et une formule 'formule'
    * Si phi est de la forme : f1 ==> (f2 ==> ... (fn ==> formule))
      la fonction conditions_apply produit la liste de conditions
      f1, f2, ... , fn
    * Si la fonction échoue elle lève une exception TacticError
    """
    typecheck_formule(phi)
    typecheck_formule(formule)

    tag, val = phi
    if tag == 'Implique':
        f1, f2 = val
        if f2 == formule:
            return [f1]
        else:
            return [f1] + conditions_apply(f2, formule)

    raise TacticError(("function conditions_apply", phi, formule))

```


```python
def apply_tactic(tactic, goal, gen_fresh_ident):
    """
    TODO : apply_tactic
    """
    typecheck_tactic(tactic)
    typecheck_goal(goal)
        
    # Tactique
    tagt, valt = tactic

    # Formule
    f = goal['formule']
    tagf, valf = f

    # Contexte
    ctx = goal['context']
    
    # Cas Tactique exact
    if tagt == 'exact':
        if valt in ctx.keys():
            if ctx[valt] == f:
                return []
        raise IdentInvalidError(('function apply_tactic exact : ' +\
             valt + ' pas dans le contexte', tactic, goal))
            
    # Cas Tactique split
    elif tagt == 'split':
        f1, f2 = valf
        if tagf == 'Et':            
            return [mk_goal(ctx, f1), mk_goal(ctx, f2)]
        elif tagf == 'Equivalent':
            return [mk_goal(ctx, ('Implique', (f1, f2))),\
                    mk_goal(ctx, ('Implique', (f2, f1)))]
   
    # Cas Tactique case
    elif tagt == 'case':
        if valt in ctx.keys():
            tag, val =  ctx[valt]
            if tag == 'Ou':
                phi1, phi2 = val
                n1 = next(gen_fresh_ident)
                n2 = next(gen_fresh_ident)
                return [mk_goal( {n1:phi1} | ctx, f),\
                    mk_goal( {n2:phi2} | ctx, f)]
        
        raise IdentInvalidError(('function apply_tactic case : '+\
             valt + ' pas dans le contexte', tactic, goal))

    # Cas Tactique decompose    
    elif tagt == 'decompose':                
        if valt in ctx.keys() :
            tag, val =  ctx[valt]
            if tag == 'Et':
                phi1, phi2 = val
                n1 = next(gen_fresh_ident)
                n2 = next(gen_fresh_ident)
                return [mk_goal({n1:phi1, n2:phi2} | ctx, f)]
        
        raise IdentInvalidError(('function apply_tactic decompose : '+\
             valt + ' pas dans le contexte', tactic, goal))

    # Cas Tactique left                                    
    elif tagt == 'left' and tagf == 'Ou':
        f1, _ = valf
        return [mk_goal(ctx, f1)]

    # Cas Tactique right
    elif tagt == 'right' and tagf == 'Ou':
        _, f2 = valf
        return [mk_goal(ctx, f2)]
        
    # Cas Tactique intro
    elif tagt == 'intro' and tagf == 'Implique':
        if valid_ident(valt, ctx):
            f1, f2 = valf
            return [mk_goal({valt:f1} | ctx, f2)]
        
        raise IdentInvalidError(('function apply_tactic intro : '+\
             valt + ' existe dans le contexte', tactic, goal))

    # Cas Tactique intros
    elif tagt == 'intros':
        if tagf == 'Implique':
            try:
                nv_c, phi = introduction(valt, ctx, f)
                return [mk_goal(nv_c, phi)]
            except TacticError as err:
                raise err

    # Cas Tactique apply    
    elif tagt == 'apply':
        if valt in ctx.keys():
            phi =  ctx[valt]
            try:
                return list(map(lambda ff : mk_goal(ctx, ff),\
                    conditions_apply(phi, f)))
            except TacticError as err:
                raise err
        
        raise IdentInvalidError(('function apply_tactic apply : '+\
             valt + ' pas dans le contexte', tactic, goal))

    # Cas Tactique absurd
    elif tagt == 'absurd':
        return [mk_goal(ctx, valt), mk_goal(ctx, ('Non', valt))]
    
    # Cas Tactique invalide
    raise TacticError(('function apply_tactic Invalid Tactic', tactic, goal))
```


```python
def step(goal, gen_fresh_ident, stringTactic=None):
    """
    * La fonction step applique une tactique tac sur le but goal
    * Si le paramètre tac vaut None la fonction est intéractive
      et demande la saisie d'une tactique.
    """
    typecheck_goal(goal)
    auto = False if (stringTactic is None) else True

    try:    
        if auto:            
            tac = analyse_tact(stringTactic)
        else:
            tac = analyse_tact(input('Saisie tactique :\n').strip()) 

    except SyntaxTacticError as err:
        if auto:
            raise err
        else:
            print('Erreur de Syntaxe')
            freshi = fresh_ident()
            return step(goal, freshi)

    except EndException as err:
        raise err

    typecheck_tactic(tac)       
    str_of_tac = string_of_tactic(tac)

    try:
        ret = apply_tactic(tac, goal, gen_fresh_ident) 

        if auto:
            print(str_of_tac)
            
        return ret

    except TacticError as err:
        if auto:
            raise err
        else:
            print('Tactique Invalide')
            freshi = fresh_ident()
            return step(goal, freshi)  

    except IdentInvalidError as err:
        if auto:
            raise err
        else:
            print('Identifiant Invalide')
            freshi = fresh_ident()
            return step(goal, freshi) 

    except :
        if auto:
            raise TacticError
        else:
            print('Erreur indéfinie')
            freshi = fresh_ident()
            return step(goal, freshi)
        

```


```python
def interactive_loop(goals, stringTactics=None):
    """
    TODO interactive_loop
    """
    auto = False if (stringTactics is None) else True
    
    for i in range(len(goals)):
        typecheck_goal(goals[i])

    if goals == []:
        return 0

    current_goal, *tail_goals = goals
    print(string_of_goal(current_goal))

    freshi = fresh_ident()

    if auto:
        tact, *tail_tact = stringTactics
        new_goals = step(current_goal, freshi, tact) + tail_goals
        return interactive_loop(new_goals, tail_tact)
    else:
        new_goals = step(current_goal, freshi) + tail_goals
        interactive_loop(new_goals)      
```


```python
def pyticoq(strformule = None, tactics = None):
    """
    """
    #auto = False if (formuleTactics is None) else True
    if strformule is None:
        try:
            formule = analyse_prop(input('Saisie Formule ?\n').strip())
            print(string_of_formule(formule))
            interactive_loop([mk_goal({}, formule)])
            print('\nQed')

        except EndException as err:
            print('Bye Bye')
            

    elif tactics is None:
        try:
            formule = analyse_prop(strformule.strip())
            print(string_of_formule(formule))
            interactive_loop([mk_goal({}, formule)])
            print('\nQed')

        except EndException as err:
            print('Bye Bye')
            

    else:
        try:
            formule = analyse_prop(strformule.strip())
            interactive_loop( [mk_goal({}, formule) ], tactics)
            print('\nQed')

        except EndException as err:
            print('Bye Bye')
            

```


```python
if __name__ == "__main__":
    parser = argparse.ArgumentParser(prog='pitycoq')
    parser.add_argument('--formule', help='formule help')
    parser.add_argument('--tactics', help='tactics help')
    args = parser.parse_args()

    if args.formule is None:
        pyticoq()
    
    elif args.tactics is None:
        pyticoq(args.formule.strip())
    
    else:
        tactics_array = list(map(lambda x : x.strip(),\
            args.tactics.split(';')))

        pyticoq(args.formule.strip(), tactics_array)
    
```
