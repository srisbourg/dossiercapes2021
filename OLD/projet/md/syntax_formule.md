# 4. Analyse Syntaxique de formules logiques
**Apprentissage de la programmation en OCaml de C. Dubois et al (2004)**, §16.3, p328

Syntaxe concrête du langage de formules


```python
from ipynb.fs.full.syntax_common import debut_mot, fin_chaine, SyntaxFormuleError, is_keyword, is_ident
```

## 4.2 Reconnaissance d'une formule
### 4.2.1 Reconnaissance de mots clés et d'identificateurs
**Apprentissage de la programmation en OCaml de C. Dubois et al (2004)**, §16.3.2.3, p335

La syntaxe concrête est définie en ASCII de la manière suivante :
* ```~``` : pour la ```Négation``` logique
* ```/\``` : pour le ```Et``` logique
* ```\/``` : pour le ```Ou``` logique
* ```==>``` : pour l'```Implication``` logique
* ```<==>``` : pour l'```Equivalence``` logique



```python
def is_VAR(s, n, pos):
    """
    La fonction is_VAR retourne une paire :
    * Une paire représantant une variable dans la syntaxe abstraite créée
      à partir de l'identificateur débutant à la position pos dans la
      chaîne s de lonhueur n.
    * La position du prochain mot dans s
    """
    assert len(s) == n
    str_v, pos_next = is_ident(s, n, pos)
    return ('Var', str_v), pos_next
```


```python
def is_LPAR(s, n, pos):
    """
    La fonction is_LPAR reconnait une parenthèse ouvrante '('
    """
    return is_keyword('(', s, n, pos)
```


```python
def is_RPAR(s, n, pos):
    """
    La fonction is_RPAR reconnait une parenthèse fermante ')'
    """
    return is_keyword(')', s, n, pos)
```


```python
def is_AND(s, n, pos):
    """
    La fonction is_AND reconnait un ET logique '/\\\\'
    """
    return is_keyword('/\\', s, n, pos)
```


```python
def is_OR(s, n, pos):
    """
    La fonction is_OR reconnait une OU logique '\\\\/'
    """
    return is_keyword('\\/', s, n, pos)
```


```python
def is_NOT(s, n, pos):
    """
    La fonction is_NOT reconnait la négation '~'
    """
    return is_keyword('~', s, n, pos)
```


```python
def is_IMP(s, n, pos):
    """
    La fonction is_IMP reconnait l'implication '==>'
    """
    return is_keyword('==>', s, n, pos)
```


```python
def is_EQU(s, n, pos):
    """
    La fonction is_EQU reconnait l'implication '<==>'
    """
    return is_keyword('<==>', s, n, pos)
```


```python
def is_Vrai(s, n, pos):
    """
    La fonction is_Vrai reconnait la valeur de vérité 'Vrai'
    """
    try:
        if is_keyword('Vrai', s, n, pos):
            return ('Vrai', None), debut_mot(s, n, pos+4)
    except :
        raise SyntaxFormuleError(('Vrai', s, n, pos))       
```


```python
def is_Faux(s, n, pos):
    """
    La fonction is_Faux reconnait la valeur de vérité 'Faux'
    """
    try:
        if is_keyword('Faux', s, n, pos):
            return ('Faux', None), debut_mot(s, n, pos+4)
    except :
        raise SyntaxFormuleError(('Faux', s, n, pos))  
```


```python
def is_BOOL(s,n, pos):
    """
    La fonction is_BOOL reconnait une valeur de vérité 'Vrai' ou 'Faux'
    """
    try:
        return is_Vrai(s,n,pos)
    except:
        return is_Faux(s,n,pos)
```

### 4.2.2 Reconnaissance de formule et priorité des opérateurs
**Apprentissage de la programmation en OCaml de C. Dubois et al (2004)**, §16.3.2.3, p332

#### Les priorités entres connexteurs sont les suivantes :

```~``` (Non) est prioritaire sur ```/\``` (Et) qui est prioritaire sur ```\/``` (Ou) qui est prioritaire sur ```==>``` (Implique) qui est prioritaire sur ```<==>``` (Equivalent).

* ```~``` (Non) est prioritaire sur ```/\``` (Et) : on lit ```~A /\ B``` comme ```(~A) /\ B``` et non comme ```~(A /\ B)```
* ```/\``` (Et) est prioritaire sur ```\/``` (Ou) : on lit ```A /\ B \/ C``` comme ```(A /\ B) \/ C``` et non comme ```A /\ (B \/ C)```
* ```\/``` (Ou) est prioritaire sur ```==>``` (Implique) : on lit ```A \/ B ==> C``` comme ```(A \/ B) ==> C``` et non comme ```A \/ (B ==> C)```
* ```==>``` (Implique) est prioritaire sur ```<==>``` (Equivalent) : on lit ```A ==> B <==> C``` comme ```(A ==> B) <==> C``` et non comme ```A ==> (B <==> C)```


#### Types de formule
* Les formules de **type 0**  comportent le connecteur ```<==>``` comme connecteur le plus externe 
* Les formules de **type 1**  comportent le connecteur ```==>``` comme connecteur le plus externe
* Les formules de **type 2**  comportent le connecteur ```\/``` comme connecteur le plus externe
* Les formules de **type 3**  comportent le connecteur ```/\``` comme connecteur le plus externe 
* Les formules de **type 4**  comportent le connecteur ```~``` comme connecteur le plus externe 
* Les formules de **type 5**  comportent le connecteur ```Vrai```, ```Faux``` ou des ```parenthèses```

#### Fonctions d'analyse de formule```is_FORM0```
Les fonctions d'analyse de formule sont **mutuellement récursives** :
* La fonction ```is_FORM_PAR``` construit  **terme parenthésé**, elle utilise la fonction ```is_FORM0```.
* La fonction ```is_FORM5``` construit un **terme parenthésé**, une valeur de vérité ```Vrai```
  ou ```Faux``` ou une ```Variable```. Elle utilise les fonctions ```is_FORM_PAR```, ```is_BOOL```
  et ```is_VAR```.
* La fonction ```is_FORM4``` construit une **négation** ou un terme généré par la fonction ```is_FORM5```.
* La fonction ```is_FORM3``` construit une **conjonction** ou un terme généré par la fonction ```is_FORM4```.
* La fonction ```is_FORM2``` construit une **disjontion** ou un terme généré par la fonction ```is_FORM3```.
* La fonction ```is_FORM1``` construit une **implication** ou un terme généré par la fonction ```is_FORM2```.
* La fonction ```is_FORM0``` construit une **équivalence** ou un terme généré par la fonction ```is_FORM1```.
* La fonction ```analyse_prop``` appelle la fonction ```is_FORM0``` pour analyser une chaîne
  et générer un tuple **tformule** de syntaxe abstraite.


```python
def analyse_prop(s):
    """
    * La fonction analyse_prop analyse une chaîne s de syntaxe concrête pour générer une
      tformule de syntaxe abstraite.
    * L'analyse est initiée par un appel à la fonction is_FORM0 à l'indice 0 de la chaîne.
    * Cet appel doit retourner une tformule f bien formée et analyser complètement
      les caractères significatifs (appel de la fonction fin_chaine à la position pos).
    * Si ce n'est pas le cas une exception SyntaxError est levée.
    """
    n = len(s)
    f, pos = is_FORM0(s,n,debut_mot(s,n,0))
    if fin_chaine(s,n,pos):
        return f
    raise SyntaxFormuleError(('function analyse_prop', s))

def is_FORM0(s,n,pos):
    """
    * La fonction is_FORM0 analyse une chaîne s de longueur n à la position pos
      pour détecter le connecteur '<==>'.
    * Elle appelle la fonction is_FORM1 à la position pos pour déterminer
      le membre gauche (f1) de la formule.
    * Elle essaye de:
      - reconnaitre la chaîne '<==>' à la position pos1
      - faire un appel récursif à partir de la position pos2
        pour calculer le membre droit (f2) de la formule 
    * Si cette analyse réussit elle retourne la paire composée de
      - la tformule ('Equivalent', (f1, f2))
      - la position pos3
    * Sinon elle retourne la paire composée de la formule f1 et de la position pos1
    """
    f1, pos1 = is_FORM1(s,n,pos)
    try:
        pos2 = is_EQU(s,n,pos1)
        f2, pos3 = is_FORM0(s,n,pos2)
        return ('Equivalent', (f1, f2)), pos3
    except:
        return f1, pos1

def is_FORM1(s,n,pos):
    """
    * La fonction is_FORM1 analyse une chaîne s de longueur n à la position pos
      pour détecter le connecteur '==>'.
    * Elle appelle la fonction is_FORM2 à la position pos pour déterminer
      le membre gauche (f1) de la formule.
    * Elle essaye de:
      - reconnaitre la chaîne '==>' à la position pos1
      - faire un appel récursif à partir de la position pos2
        pour calculer le membre droit (f2) de la formule 
    * Si cette analyse réussit elle retourne la paire composée de
      - la tformule ('Implique', (f1, f2))
      - la position pos3
    * Sinon elle retourne la paire composée de la formule f1 et de la position pos1
    """
    f1, pos1 = is_FORM2(s,n,pos)
    try:
        pos2 = is_IMP(s,n,pos1)
        f2, pos3 = is_FORM1(s,n,pos2)
        return ('Implique', (f1, f2)), pos3
    except:
        return f1, pos1


def is_FORM2(s,n,pos):
    """
    * La fonction is_FORM2 analyse une chaîne s de longueur n à la position pos
      pour détecter le connecteur '\/'.
    * Elle appelle la fonction is_FORM3 à la position pos pour déterminer
      le membre gauche (f1) de la formule.
    * Elle essaye de:
      - reconnaitre la chaîne '\/' à la position pos1
      - faire un appel récursif à partir de la position pos2
        pour calculer le membre droit (f2) de la formule 
    * Si cette analyse réussit elle retourne la paire composée de
      - la tformule ('Ou', (f1, f2))
      - la position pos3
    * Sinon elle retourne la paire composée de la formule f1 et de la position pos1
    """
    f1, pos1 = is_FORM3(s,n,pos)
    try:
        pos2 = is_OR(s, n, pos1)
        f2, pos3 = is_FORM2(s,n,pos2)
        return ('Ou', (f1, f2)), pos3
    except:
        return f1, pos1

def is_FORM3(s,n,pos):
    """
    * La fonction is_FORM3 analyse une chaîne s de longueur n à la position pos
      pour détecter le connecteur '/\\'.
    * Elle appelle la fonction is_FORM4 à la position pos pour déterminer
      le membre gauche (f1) de la formule.
    * Elle essaye de:
      - reconnaitre la chaîne '/\\' à la position pos1
      - faire un appel récursif à partir de la position pos2
        pour calculer le membre droit (f2) de la formule 
    * Si cette analyse réussit elle retourne la paire composée de
      - la tformule ('Et', (f1, f2))
      - la position pos3
    * Sinon elle retourne la paire composée de la formule f1 et de la position pos1
    """
    f1, pos1 = is_FORM4(s,n,pos)
    try:
        pos2 = is_AND(s,n,pos1)
        f2, pos3 = is_FORM3(s,n,pos2)
        return ('Et', (f1, f2)), pos3
    except:
        return f1, pos1

def is_FORM4(s,n,pos):
    """
    * La fonction is_FORM4 analyse une chaîne s de longueur n à la position pos
      pour détecter le connecteur '~'.
    * Elle essaye de
      - reconnaitre la chaîne '~' à la position pos
      - faire un appel récursif à partir de la position pos1
        pour calculer le membre droit (f) de la formule 
    * Si cette analyse réussit elle retourne la paire composée de
      - la tformule ('Non', f)
      - la position pos
    * Sinon elle appelle la fonction is_FORM5 appliquée à la position pos
    """
    try:
        pos1 = is_NOT(s,n,pos)
        f, pos2 = is_FORM4(s,n,pos1)
        return ('Non', f), pos2
    except:
        return is_FORM5(s,n,pos)

def is_FORM5(s,n, pos):
    """
    * La fonction is_FORM5 analyse une chaîne s de longueur n à la position pos
      pour détecter :
      - Un terme parenthèsé
      - Un littéral 'Vrai' ou 'Faux'
      - Un identifiant de variable
    * Elle appelle la fonction is_FORM_PAR pour essayer de reconnaitre
      un terme terme parenthèsé à partir de la position pos.
    * Si cette analyse réussit elle retourne la tformule produite
    * Sinon elle essaye de reconnaitre un littéral 'Vrai' ou 'Faux'
      à partir de la position pos et retourne la tformule ('Vrai', None)
      ou ('Faux', None) correspondante.
    * Sinon elle elle construit une variable de la forme ('Var', '<IDENTIFIANT>')
    """
    try:
        return is_FORM_PAR(s,n, pos)
    except:
        try:
            return is_BOOL(s,n, pos)
        except:
            return is_VAR(s,n,pos)

def is_FORM_PAR(s,n,pos):
    """
    * La fonction is_FORM_PAR analyse une chaîne s de longueur n à la position pos
      pour détecter un terme parenthèsé '( <tformule> )'.
    * On cherche une parenthèse ouvrante '(' à la position pos
    * Par l'appel à la fonction is_FORM0 on cherche le terme f mis
      entre parenthèse à la position pos1
    * On cherche finalement la parenthèse fermante ')' à la position pos2
    * On retourne la paire constituée de :
      - la tformule f
      - la position pos3
    """
    pos1 = is_LPAR(s,n,pos)
    f, pos2 = is_FORM0(s,n,pos1)
    pos3 = is_RPAR(s,n,pos2)
    return f, pos3
```
