# 7. But d'une preuve


```
"""
Module de définition et de vérification du type goal
"""

from ipynb.fs.full.type_context import TContextError, typecheck_context
from ipynb.fs.full.type_formule import typecheck_formule, TypeCheckFormuleError
```


```
def is_tgoal(goal):
    """
    * La fonction is_tgoal vérifie si le paramètre 
      tgoal est bien un dictionnaire qui contient:
      - Une clé 'context' dont la valeur est 
        un contexte
      - Une clé 'formule' dont la valeur est une formule
    * Elle retourne une paire constitué d'un booléen
      et de la variable goal convertie en chaîne
      de caractères.
    """
    if not isinstance(goal, dict):
        return False

    if 'context' not in goal.keys():
        return False

    if 'formule' not in goal.keys():
        return False

    try:
        typecheck_formule(goal['formule'])
    except TypeCheckFormuleError as err:
        raise err

    try:
        typecheck_context(goal['context'])
    except TContextError as err:
        raise err

    return True
        
```


```

class TGoalError(Exception):
    """
    Exception TypeCheckFormuleError pour la fonction typecheck_context
    """
    def __init__(self, value):
        self.value = value
        Exception.__init__(self)
    def __str__(self):
        return repr(self.value)

def typecheck_goal(goal):
    """
    * La fonction typecheck_goal vérifie si le 
      paramètre goal est bien un but.
    * Elle lance une exception TGoalError si ce n'est
      pas le cas.
    """
    try:
        if not is_tgoal(goal):
            raise TGoalError(goal)
    except TypeCheckFormuleError as err:
        raise TGoalError(goal) from err
    except TContextError as err:
        raise TGoalError(goal) from err
```


```
def mk_goal(context, formule):
    """
    La fonction mk_goal construit un but (goal) à partir
    d'une formule et d'un contexte.
    """
    typecheck_context(context)
    typecheck_formule(formule)
    ret = {'context':context, 'formule':formule}
    typecheck_goal(ret)        
    return ret
```
