import sys
import os

sys.path.insert(0, os.path.join('..', 'lib'))

from type_tactic import *
from unit_test_common import *


debut_test_module('type_tactic')



# Tests type_tactic.is_ttactic
debut_test_fonction('is_ttactic')
print(is_ttactic.__doc__)
is_ttactic_count = gen_int_count()

tt = ('split', None)
test_if(next(is_ttactic_count), is_ttactic(tt), 'is_ttactic')

tt = ('left', None)
test_if(next(is_ttactic_count), is_ttactic(tt), 'is_ttactic')

tt = ('right', None)
test_if(next(is_ttactic_count), is_ttactic(tt), 'is_ttactic')

tt = ('exact', 'None')
test_if(next(is_ttactic_count), is_ttactic(tt), 'is_ttactic')

tt = ('decompose', 'None')
test_if(next(is_ttactic_count), is_ttactic(tt), 'is_ttactic')

tt = ('case', 'None')
test_if(next(is_ttactic_count), is_ttactic(tt), 'is_ttactic')

tt = ('intro', 'None')
test_if(next(is_ttactic_count), is_ttactic(tt), 'is_ttactic')

tt = ('apply', 'None')
test_if(next(is_ttactic_count), is_ttactic(tt), 'is_ttactic')

tt = 'absurd', ('Vrai', None)
test_if(next(is_ttactic_count), is_ttactic(tt), 'is_ttactic')

tt = 'intros', ['a', 'b']
test_if(next(is_ttactic_count), is_ttactic(tt), 'is_ttactic')

test_exception(next(is_ttactic_count), lambda x : is_ttactic('test', None), 'is_ttactic')

fin_test_fonction('is_ttactic')



# Tests type_tactic.typecheck_tactic
debut_test_fonction('typecheck_tactic')
print(typecheck_tactic.__doc__)
typecheck_tactic_count = gen_int_count()

tt = ('split', None)
test_no_exception(next(typecheck_tactic_count), lambda x : typecheck_tactic(tt), 'typecheck_tactic')

tt = ('left', None)
test_no_exception(next(typecheck_tactic_count), lambda x : typecheck_tactic(tt), 'typecheck_tactic')

tt = ('right', None)
test_no_exception(next(typecheck_tactic_count), lambda x : typecheck_tactic(tt), 'typecheck_tactic')

tt = ('exact', 'None')
test_no_exception(next(typecheck_tactic_count), lambda x : typecheck_tactic(tt), 'typecheck_tactic')

tt = ('decompose', 'None')
test_no_exception(next(typecheck_tactic_count), lambda x : typecheck_tactic(tt), 'typecheck_tactic')

tt = ('case', 'None')
test_no_exception(next(typecheck_tactic_count), lambda x : typecheck_tactic(tt), 'typecheck_tactic')

tt = ('intro', 'None')
test_no_exception(next(typecheck_tactic_count), lambda x : typecheck_tactic(tt), 'typecheck_tactic')

tt = ('apply', 'None')
test_no_exception(next(typecheck_tactic_count), lambda x : typecheck_tactic(tt), 'typecheck_tactic')

tt = ('absurd', ('Vrai', None))
test_no_exception(next(typecheck_tactic_count), lambda x : typecheck_tactic(tt), 'typecheck_tactic')

tt = ('intros', ['a', 'b'])
test_no_exception(next(typecheck_tactic_count), lambda x : typecheck_tactic(tt), 'typecheck_tactic')

tt = ('test', None)
test_exception(next(typecheck_tactic_count), lambda x : typecheck_tactic(tt), 'typecheck_tactic')


fin_test_fonction('typecheck_tactic')

# Modèle test
# debut_test_fonction('FONCTION')
# print(FONCTION.__doc__)
# FONCTION_count = gen_int_count()
# fin_test_fonction('FONCTION')


fin_test_module('type_tactic')