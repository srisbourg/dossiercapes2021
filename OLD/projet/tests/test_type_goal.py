
import sys
import os

sys.path.insert(0, os.path.join('..', 'lib'))

from type_goal import *
from unit_test_common import *

debut_test_module('type_goal')

# Tests type_context.is_tgoal
debut_test_fonction('is_tgoal')
print(is_tgoal.__doc__)
is_tgoal_count = gen_int_count()

b= is_tgoal('df')
test_ifnot(next(is_tgoal_count), b, 'is_tgoal')


test_exception(next(is_tgoal_count),\
    lambda x : is_tgoal({'context':'df', 'formule':None}),\
        'is_tgoal')

test_exception(next(is_tgoal_count),\
    lambda x : is_tgoal({'context':'df', 'formule':('Vrai',None)}),\
        'is_tgoal')

test_exception(next(is_tgoal_count),\
    lambda x : is_tgoal({'context':['df'], 'formule':('Vrai',None)}),\
        'is_tgoal')

test_exception(next(is_tgoal_count),\
    lambda x : is_tgoal({'context':[('df', None)], 'formule':('Vrai',None)}),\
        'is_tgoal')


b = is_tgoal({'context':{'df': ('Vrai', None)}, 'formule':('Vrai',None)})
test_if(next(is_tgoal_count), b, 'is_tgoal')

b= is_tgoal({'context':{'df': ('Vrai', None), 'd':('Faux', None)}, 'formule':('Vrai',None)})
test_if(next(is_tgoal_count), b, 'is_tgoal')

fin_test_fonction('is_tgoal')

# Tests type_context.typecheck_goal
debut_test_fonction('typecheck_goal')
print(typecheck_goal.__doc__)
typecheck_goal_count = gen_int_count()

g = 'df'
test_exception(next(typecheck_goal_count), lambda x : typecheck_goal(g), 'typecheck_goal')

g = {'context':'df', 'formule':None}
test_exception(next(typecheck_goal_count), lambda x : typecheck_goal(g), 'typecheck_goal')

g = {'context':'df', 'formule':('Vrai',None)}
test_exception(next(typecheck_goal_count), lambda x : typecheck_goal(g), 'typecheck_goal')

g = {'context':['df'], 'formule':('Vrai',None)}
test_exception(next(typecheck_goal_count), lambda x : typecheck_goal(g), 'typecheck_goal')

g = {'context':[('df', None)], 'formule':('Vrai',None)}
test_exception(next(typecheck_goal_count), lambda x : typecheck_goal(g), 'typecheck_goal')

g = {'context':{'df': ('Vrai', None)}, 'formule':('Vrai',None)}
test_no_exception(next(typecheck_goal_count), lambda x : typecheck_goal(g), 'typecheck_goal')

g = {'context':{'df': ('Vrai', None), 'd':('Faux', None)}, 'formule':('Vrai',None)}
test_no_exception(next(typecheck_goal_count), lambda x : typecheck_goal(g), 'typecheck_goal')

fin_test_fonction('typecheck_goal')


# Tests type_goal.mk_goal
debut_test_fonction('mk_goal')
print(mk_goal.__doc__)
mk_goal_count = gen_int_count()

context = {'df': ('Vrai', None)}
formule = ('Faux',None)
oracle = {'context':context, 'formule':formule}

test_if(next(mk_goal_count), mk_goal(context, formule)==oracle, 'mk_goal')

fin_test_fonction('mk_goal')

# Modèle test
# debut_test_fonction('FONCTION')
# print(FONCTION.__doc__)
# FONCTION_count = gen_int_count()
#
#
# fin_test_fonction('FONCTION')

fin_test_module('type_goal')