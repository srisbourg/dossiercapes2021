import sys
import os

sys.path.insert(0, os.path.join('..', 'lib'))

from type_formule import *
from syntax_formule import *
from printer import *
from unit_test_common import *
from type_tactic import *

debut_test_module('printer')

# Tests printer.formule_priority
debut_test_fonction('formule_priority')
print(formule_priority.__doc__)
formule_priority_count = gen_int_count()

test_if(next(formule_priority_count),\
    formule_priority(analyse_prop('P <==> Q')) == 1,
    'formule_priority')

test_if(next(formule_priority_count),\
    formule_priority(analyse_prop('P ==> Q')) == 2,
    'formule_priority')

test_if(next(formule_priority_count),\
    formule_priority(analyse_prop('P \/ Q')) == 3,
    'formule_priority')

test_if(next(formule_priority_count),\
    formule_priority(analyse_prop('P /\ Q')) == 4,
    'formule_priority')

test_if(next(formule_priority_count),\
    formule_priority(analyse_prop('~(P ==> Q)')) == 5,
    'formule_priority')

test_if(next(formule_priority_count),\
    formule_priority(analyse_prop('P')) == 6,
    'formule_priority')    

test_if(next(formule_priority_count),\
    formule_priority(analyse_prop('Vrai')) == 6,
    'formule_priority')

fin_test_fonction('formule_priority')

# Tests printer.string_of_formule
debut_test_fonction('string_of_formule')
print(string_of_formule.__doc__)
string_of_formule_count = gen_int_count()

p = "~(P==>Q)"
oracle = '~(P ==> Q)'
s = string_of_formule(analyse_prop(p))
test_if(next(string_of_formule_count), s == oracle, 'string_of_formule')

p = "((P<==>Q) /\\ R)"
oracle = '(P <==> Q) /\\ R'
s = string_of_formule(analyse_prop(p))
test_if(next(string_of_formule_count), s == oracle, 'string_of_formule')

p = "((P==>Q) ==> R)"
oracle = '(P ==> Q) ==> R'
s = string_of_formule(analyse_prop(p))
test_if(next(string_of_formule_count), s == oracle, 'string_of_formule')

p = "(P==>Q ==> R)"
oracle = 'P ==> Q ==> R'
s = string_of_formule(analyse_prop(p))
test_if(next(string_of_formule_count), s == oracle, 'string_of_formule')

p = "~~(P==>Q ==> R)"
oracle = '~~(P ==> Q ==> R)'
s = string_of_formule(analyse_prop(p))
test_if(next(string_of_formule_count), s == oracle, 'string_of_formule')

fin_test_fonction('string_of_formule')

# Tests printer.string_of_hyp
debut_test_fonction('string_of_hyp')
print(string_of_hyp.__doc__)
string_of_hyp_count = gen_int_count()

p = analyse_prop("((P==>Q) ==> R)")
oracle = 'toto : (P ==> Q) ==> R'

test_if(next(string_of_hyp_count), string_of_hyp('toto', p) == oracle, 'string_of_hyp')

fin_test_fonction('string_of_hyp')

# Tests printer.string_of_context
debut_test_fonction('string_of_context')
print(string_of_context.__doc__)
string_of_context_count = gen_int_count()

h0 = analyse_prop("(P==>Q ==> R)")
h1 = analyse_prop("~~(P==>Q ==> R)")
context = { 'h0' : h0,\
            'h1' : h1}

oracle = string_of_hyp('h0', h0) + '\n' + string_of_hyp('h1', h1) + '\n'

test_if(next(string_of_context_count), string_of_context(context)==oracle, 'string_of_context')

fin_test_fonction('string_of_context')

# Tests printer.string_of_goal
debut_test_fonction('string_of_goal')
print(string_of_goal.__doc__)
string_of_goal_count = gen_int_count()

theo = analyse_prop('(Q==>R)==>R')
ctx = {'H': analyse_prop('P==>Q'), 'Hp': analyse_prop('P')}
goal={'formule':theo, 'context':ctx}

oracle = '\n' + string_of_context(ctx) + ('=' * 50) + '\n' + string_of_formule(theo) + '\n'

test_if(next(string_of_goal_count), string_of_goal(goal)==oracle, 'string_of_goal')

fin_test_fonction('string_of_goal')

# Tests printer.fresh_ident
debut_test_fonction('fresh_ident')
print(fresh_ident.__doc__)
fresh_ident_count = gen_int_count()

fi = fresh_ident()

for i in range(5):
    test_if(next(fresh_ident_count), next(fi) == '_hyp' + str(i), 'fresh_ident')

fi = fresh_ident('toto')

for i in range(5):
    test_if(next(fresh_ident_count), next(fi) == 'toto' + str(i), 'fresh_ident')

fin_test_fonction('fresh_ident')

# Tests printer.string_of_tactic
debut_test_fonction('string_of_tactic')
print(string_of_tactic.__doc__)
string_of_tactic_count = gen_int_count()

# split
test_if(next(string_of_tactic_count), string_of_tactic(('split', None))=='split','string_of_tactic', 'split') 

# left
test_if(next(string_of_tactic_count), string_of_tactic(('left', None))=='left','string_of_tactic', 'left') 

# right
test_if(next(string_of_tactic_count), string_of_tactic(('right', None))=='right','string_of_tactic', 'right') 

# exact, decompose, case, intro, apply
for tact in ('exact', 'decompose', 'case', 'intro', 'apply'):
    test_if(next(string_of_tactic_count), string_of_tactic((tact, 'test'))==tact + ' test','string_of_tactic', tact) 

#'absurd'
test_if(next(string_of_tactic_count), string_of_tactic(('absurd', analyse_prop('P==>Q')))=='absurd ' + string_of_formule(analyse_prop('P==>Q')) ,'string_of_tactic', 'absurd') 

test_exception2(next(string_of_tactic_count), lambda x : string_of_tactic(('absurd', ('Coucou', None))),\
        'absurd', TypeCheckTacticError, 'TypeCheckTacticError')

#'intros'
test_if(next(string_of_tactic_count), string_of_tactic(('intros', ['h0'] ))=='intros h0','string_of_tactic', 'intros') 

test_if(next(string_of_tactic_count), string_of_tactic(('intros', ['h0', 'h1', 'h2'] ))=='intros h0, h1, h2','string_of_tactic', 'intros') 

test_exception2(next(string_of_tactic_count), lambda x : string_of_tactic(('intros', [] )),\
        'intros', TypeCheckTacticError, 'TypeCheckTacticError')

#Invalid
test_exception2(next(string_of_tactic_count), lambda x : string_of_tactic(('Coucou', [] )),\
        'Invalid', TypeCheckTacticError, 'TypeCheckTacticError')


fin_test_fonction('string_of_tactic')

# Modèle test
# debut_test_fonction('FONCTION')
# print(FONCTION.__doc__)
# FONCTION_count = gen_int_count()
# fin_test_fonction('FONCTION')

fin_test_module('printer')