import sys
import os

sys.path.insert(0, os.path.join('..', 'lib'))

from syntax_common import *
from unit_test_common import *

debut_test_module('syntax_common')

# Tests syntax_common.ignore_char
debut_test_fonction('ignore_char')
print(ignore_char.__doc__)
igch_count = gen_int_count()

test_if(next(igch_count), ignore_char(' '), 'ignore_char', "Test ' '")
test_if(next(igch_count), ignore_char('\n'), 'ignore_char', "Test '\\n'")
test_if(next(igch_count), ignore_char('\t'), 'ignore_char', "Test '\\t'")
test_ifnot(next(igch_count), ignore_char('4'), 'ignore_char', "Test '4'")

fin_test_fonction('ignore_char')


# Tests syntax_common.debut_mot
debut_test_fonction('debut_mot')
print(debut_mot.__doc__)
debmot_count = gen_int_count()

test_if(next(debmot_count), debut_mot('P /\ Q', 6, 1) == 2, 'debut_mot', "1")
test_if(next(debmot_count), debut_mot('P /\ Q', 6, 4) == 5, 'debut_mot', "2")

fin_test_fonction('debut_mot')


# Tests syntax_common.fin_chaine
debut_test_fonction('fin_chaine')
print(fin_chaine.__doc__)
finch_count = gen_int_count()

test_if(next(finch_count), fin_chaine('P /\ Q', 6, 6 ), 'debut_mot', "fin_chaine('P /\ Q', 6, 6 )")
test_if(next(finch_count), fin_chaine('P /\ Q    ', 10, 6 ), 'debut_mot', "fin_chaine('P /\ Q    ', 10, 6 )")

fin_test_fonction('fin_chaine')

# TODO tests keyword

# Tests syntax_common.possible
debut_test_fonction('possible')
print(possible.__doc__)
poss_count = gen_int_count()

test_if(next(poss_count), possible('_'), 'possible', "possible('_')")
test_if(next(poss_count), possible('D'), 'possible', "possible('D')")
test_if(next(poss_count), possible('f'), 'possible', "possible('f')")
test_if(next(poss_count), possible('4'), 'possible', "possible('4')")
test_ifnot(next(poss_count), possible(';'), 'possible', "possible(';')")

fin_test_fonction('possible')


# Tests syntax_common.is_ident
debut_test_fonction('is_ident')
print(is_ident.__doc__)
id_count = gen_int_count()

s='dkfjsdklf   kfsdkf'
assert(len(s)==18)

test_if(next(id_count), is_ident(s, len(s), 0) == ('dkfjsdklf', 12), 'ident')
test_if(next(id_count), is_ident(s, len(s), 12) == ('kfsdkf', 18), 'ident')

fin_test_fonction('is_ident')

fin_test_module('syntax_common')