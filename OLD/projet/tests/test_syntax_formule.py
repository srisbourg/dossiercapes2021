import sys
import os

sys.path.insert(0, os.path.join('..', 'lib'))

from type_formule import *
from interpret_formule import *
from syntax_formule import *
from unit_test_common import *

debut_test_module('syntax_formule')

# Test syntax_formule.mkVAR
debut_test_fonction('is_VAR')
print(is_VAR.__doc__)
mkV_count = gen_int_count()

s=' dfsd dsfkj kjlk  '
assert(len(s) == 18)
test_if(next(mkV_count), is_VAR(s, 18, 1) == (('Var', 'dfsd'), 6), 'is_VAR')
test_if(next(mkV_count), is_VAR(s, 18, 6) == (('Var', 'dsfkj'), 12), 'is_VAR')
test_if(next(mkV_count), is_VAR(s, 18, 12) == (('Var', 'kjlk'), 18), 'is_VAR')

fin_test_fonction('is_VAR')


# Tests syntax_formule.is_LPAR
debut_test_fonction('is_LPAR')
print(is_LPAR.__doc__)
is_LPAR_count = gen_int_count()

s = 'sdkfj ( kdfls (dsf'
assert(len(s) == 18)
test_if(next(is_LPAR_count), is_LPAR(s, 18, 6), 'is_LPAR')
test_if(next(is_LPAR_count), is_LPAR(s, 18, 14), 'is_LPAR')


fin_test_fonction('is_LPAR')


# Tests syntax_formule.is_RPAR
debut_test_fonction('is_RPAR')
print(is_RPAR.__doc__)
is_RPAR_count = gen_int_count()

s = 'sdkfj ) kdfls )dsf'
assert(len(s) == 18)
test_if(next(is_RPAR_count), is_RPAR(s, 18, 6), 'is_RPAR')
test_if(next(is_RPAR_count), is_RPAR(s, 18, 14), 'is_RPAR')

fin_test_fonction('is_RPAR')


# Tests syntax_formule.is_AND
debut_test_fonction('is_AND')
print(is_AND.__doc__)
is_AND_count = gen_int_count()
test_if(next(is_AND_count), is_AND('/\\', 2, 0), 'is_AND')
test_if(next(is_AND_count), is_AND('P /\\ Q', 6, 2), 'is_AND')

fin_test_fonction('is_AND')


# Tests syntax_formule.is_OR
debut_test_fonction('is_OR')
print(is_OR.__doc__)
is_OR_count = gen_int_count()
test_if(next(is_OR_count), is_OR('\\/', 2, 0), 'is_OR')
test_if(next(is_OR_count), is_OR('P \\/ Q', 6, 2), 'is_OR')

fin_test_fonction('is_OR')


# Tests syntax_formule.is_NOT
debut_test_fonction('is_NOT')
print(is_NOT.__doc__)
is_NOT_count = gen_int_count()

s = 'dkfj~sfdkln ~ldf    ~ dsf'

test_if(next(is_NOT_count), is_NOT(s, 25, 4), 'is_NOT')
test_if(next(is_NOT_count), is_NOT(s, 25, 12), 'is_NOT')
test_if(next(is_NOT_count), is_NOT(s, 25, 20), 'is_NOT')

fin_test_fonction('is_NOT')


# Tests syntax_formule.is_IMP
debut_test_fonction('is_IMP')
print(is_IMP.__doc__)
is_IMP_count = gen_int_count()

s = 'dkfj==>sfdkln ==>ldf    ==> dsf'

test_if(next(is_IMP_count), is_IMP(s, 31, 4), 'is_IMP')
test_if(next(is_IMP_count), is_IMP(s, 31, 14), 'is_IMP')
test_if(next(is_IMP_count), is_IMP(s, 31, 24), 'is_IMP')

fin_test_fonction('is_IMP')


# Tests syntax_formule.is_EQU
debut_test_fonction('is_EQU')
print(is_EQU.__doc__)
is_EQU_count = gen_int_count()

s = 'dkfj<==>sfdkln <==>ldf    <==> dsf'

test_if(next(is_EQU_count), is_EQU(s, 34, 4), 'is_EQU')
test_if(next(is_EQU_count), is_EQU(s, 34, 15), 'is_EQU')
test_if(next(is_EQU_count), is_EQU(s, 34, 26), 'is_EQU')

fin_test_fonction('is_EQU')


# Tests syntax_formule.is_Vrai
debut_test_fonction('is_Vrai')
print(is_Vrai.__doc__)
is_Vrai_count = gen_int_count()

s = ' dskflskd Vrai dsf Vrai'
assert(len(s)==23)
test_if(next(is_Vrai_count), is_Vrai(s, 23, 10), 'is_Vrai')

v, p = is_Vrai(s, 23, 10)
test_no_exception(next(is_Vrai_count), lambda x : typecheck_formule(v), 'is_Vrai')
test_if(next(is_Vrai_count), v==('Vrai', None), 'is_Vrai')
test_if(next(is_Vrai_count), p==15, 'is_Vrai')
test_if(next(is_Vrai_count), is_Vrai(s, 23, 19), 'is_Vrai')

fin_test_fonction('is_Vrai')


# Tests syntax_formule.is_Faux
debut_test_fonction('is_Faux')
print(is_Faux.__doc__)
is_Faux_count = gen_int_count()

s = ' dskflskd Faux dsf Faux'
assert(len(s)==23)
test_if(next(is_Faux_count), is_Faux(s, 23, 10), 'is_Faux')

v, p = is_Faux(s, 23, 10)
test_no_exception(next(is_Faux_count), lambda x : typecheck_formule(v), 'is_Faux')
test_if(next(is_Faux_count), v==('Faux', None), 'is_Faux')
test_if(next(is_Faux_count), p==15, 'is_Faux')
test_if(next(is_Faux_count), is_Faux(s, 23, 19), 'is_Faux')

fin_test_fonction('is_Faux')


# Tests syntax_formule.is_BOOL
debut_test_fonction('is_BOOL')
print(is_BOOL.__doc__)
is_BOOL_count = gen_int_count()

s = ' dskflskd Faux dsf Vrai'
assert(len(s)==23)
test_if(next(is_BOOL_count), is_BOOL(s, 23, 10), 'is_BOOL')

v, p = is_BOOL(s, 23, 10)
test_no_exception(next(is_BOOL_count), lambda x : typecheck_formule(v), 'is_BOOL')
test_if(next(is_BOOL_count), v==('Faux', None), 'is_BOOL')
test_if(next(is_BOOL_count), p==15, 'is_BOOL')
test_if(next(is_BOOL_count), is_BOOL(s, 23, 19), 'is_BOOL')

fin_test_fonction('is_BOOL')


# Tests interpret syntax_formule.is_FORM0
debut_test_fonction('is_FORM0')
print(is_FORM0.__doc__)
is_FORM0_count = gen_int_count()

str_form = 'P1 <==> Q'
assert(len(str_form)==9)

res = 'Equivalent', (('Var', 'P1'), ('Var', 'Q'))

test_if(next(is_FORM0_count), is_FORM0(str_form, 9, 0) == (res, 9), 'is_FORM0' )

fin_test_fonction('is_FORM0')


# Tests interpret syntax_formule.is_FORM1
debut_test_fonction('is_FORM1')
print(is_FORM1.__doc__)
is_FORM1_count = gen_int_count()

str_form = 'P1 ==> Q'
assert(len(str_form)==8)

res = 'Implique', (('Var', 'P1'), ('Var', 'Q'))

test_if(next(is_FORM1_count), is_FORM1(str_form, 8, 0) == (res , 8), 'is_FORM1' )

fin_test_fonction('is_FORM1')


# Tests interpret syntax_formule.is_FORM2
debut_test_fonction('is_FORM2')
print(is_FORM2.__doc__)
is_FORM2_count = gen_int_count()

str_form = 'P1 \/ Q'
assert(len(str_form)==7)

res = 'Ou', (('Var', 'P1'), ('Var', 'Q'))

test_if(next(is_FORM2_count), is_FORM2(str_form, 7, 0) == (res, 7), 'is_FORM2' )

fin_test_fonction('is_FORM2')


# Tests interpret syntax_formule.is_FORM3
debut_test_fonction('is_FORM3')
print(is_FORM3.__doc__)
is_FORM3_count = gen_int_count()

str_form = 'P1 /\\ Q'
assert(len(str_form)==7)

res = 'Et', (('Var', 'P1'), ('Var', 'Q'))

#print(str(is_FORM3(str_form, 7, 0) ))

test_if(next(is_FORM3_count), is_FORM3(str_form, 7, 0) == (res, 7), 'is_FORM3' )

fin_test_fonction('is_FORM3')


# Tests interpret syntax_formule.is_FORM4
debut_test_fonction('is_FORM4')
print(is_FORM4.__doc__)
is_FORM4_count = gen_int_count()

str_form = '(~P1 \/ P2) /\\ Q'
assert(len(str_form)==16)

#print(str(is_FORM4(str_form, 16, 1) ))
res = 'Ou', (('Non', ('Var', 'P1')), ('Var', 'P2'))

test_if(next(is_FORM4_count), is_FORM4(str_form, 16, 1) == (('Non', ('Var', 'P1')), 5), 'is_FORM4' )
test_if(next(is_FORM4_count), is_FORM4(str_form, 16, 0) == (res, 12), 'is_FORM4' )

fin_test_fonction('is_FORM4')


# Tests interpret syntax_formule.IS_FORM5
debut_test_fonction('is_FORM5')
print(is_FORM5.__doc__)
is_FORM5_count = gen_int_count()

str_form = '(P1 \/ Vrai) /\\ Q'
assert(len(str_form)==17)

res0 = 'Ou', (('Var', 'P1'), ('Vrai', None))

#print(str(is_FORM5(str_form, 17, 0)))

test_if(next(is_FORM5_count), is_FORM5(str_form, 17, 16) == (('Var', 'Q'), 17) , 'IS_FORM5' )
test_if(next(is_FORM5_count), is_FORM5(str_form, 17, 7) == (('Vrai', None), 11) , 'IS_FORM5' )
test_if(next(is_FORM5_count), is_FORM5(str_form, 17, 0) == (res0, 13) , 'IS_FORM5' )

fin_test_fonction('is_FORM5')


# Tests interpret syntax_formule.is_FORM_PAR
debut_test_fonction('is_FORM_PAR')
print(is_FORM_PAR.__doc__)
is_FORM_PAR_count = gen_int_count()

str_form = '(~P1 \/ P2) /\\ Q'
assert(len(str_form)==16)

res = 'Ou', (('Non', ('Var', 'P1')), ('Var', 'P2'))

test_if(next(is_FORM_PAR_count), is_FORM_PAR(str_form, 16, 0) == (res, 12), 'is_FORM_PAR' )

fin_test_fonction('is_FORM_PAR')


# Tests interpret syntax_formule.analyse_prop
debut_test_fonction('analyse_prop')
print(analyse_prop.__doc__)
analyse_prop_count = gen_int_count()

ret = analyse_prop('P1         ')
test_no_exception(next(analyse_prop_count), lambda x : test_no_exception(next(analyse_prop_count), lambda x : typecheck_formule(ret), 'analyse_prop' ), 'analyse_prop' )
oracle = ('Var', 'P1')
test_if(next(analyse_prop_count), ret == oracle, 'analyse_prop' )

ret = analyse_prop('          _S')
test_no_exception(next(analyse_prop_count), lambda x : typecheck_formule(ret), 'analyse_prop' )
oracle = ('Var', '_S')
test_if(next(analyse_prop_count), ret == oracle, 'analyse_prop' )

ret = analyse_prop('      (E)    ')
test_no_exception(next(analyse_prop_count), lambda x : typecheck_formule(ret), 'analyse_prop' )
oracle = ('Var', 'E')
test_if(next(analyse_prop_count), ret == oracle, 'analyse_prop' )

ret = analyse_prop('      ~(E)    ')
test_no_exception(next(analyse_prop_count), lambda x : typecheck_formule(ret), 'analyse_prop' )
oracle = ('Non', ('Var', 'E'))
test_if(next(analyse_prop_count), ret == oracle, 'analyse_prop' )

ret = analyse_prop(' ~~E    ')
test_no_exception(next(analyse_prop_count), lambda x : typecheck_formule(ret), 'analyse_prop' )
oracle = ('Non', ('Non', ('Var', 'E')))
test_if(next(analyse_prop_count), ret == oracle, 'analyse_prop' )

ret = analyse_prop('P1 \n /\\ Q')
test_no_exception(next(analyse_prop_count), lambda x : typecheck_formule(ret), 'analyse_prop' )
oracle = ('Et', (('Var', 'P1'), ('Var', 'Q')))
test_if(next(analyse_prop_count), ret == oracle, 'analyse_prop' )

ret = analyse_prop('P1 \n /\\ Q')
test_no_exception(next(analyse_prop_count), lambda x : typecheck_formule(ret), 'analyse_prop' )
oracle = ('Et', (('Var', 'P1'), ('Var', 'Q')))
test_if(next(analyse_prop_count), ret == oracle, 'analyse_prop' )

ret = analyse_prop('~E/\\P')
test_no_exception(next(analyse_prop_count), lambda x : typecheck_formule(ret), 'analyse_prop' )
oracle = ('Et', (('Non', ('Var', 'E')), ('Var', 'P')))
test_if(next(analyse_prop_count), ret == oracle, 'analyse_prop' )

ret = analyse_prop('P/\\Q \\/ S')
test_no_exception(next(analyse_prop_count), lambda x : typecheck_formule(ret), 'analyse_prop' )
oracle = ('Ou', (('Et', (('Var', 'P'), ('Var', 'Q'))), ('Var', 'S')))
test_if(next(analyse_prop_count), ret == oracle, 'analyse_prop' )

ret = analyse_prop(' P/\\ Q /\\ S')
test_no_exception(next(analyse_prop_count), lambda x : typecheck_formule(ret), 'analyse_prop' )
oracle = ('Et', (('Var', 'P'), ('Et', (('Var', 'Q'), ('Var', 'S')))))
test_if(next(analyse_prop_count), ret == oracle, 'analyse_prop' )

ret = analyse_prop(' P\\/ Q /\\ S')
test_no_exception(next(analyse_prop_count), lambda x : typecheck_formule(ret), 'analyse_prop' )
oracle = ('Ou', (('Var', 'P'), ('Et', (('Var', 'Q'), ('Var', 'S')))))
test_if(next(analyse_prop_count), ret == oracle, 'analyse_prop' )

ret = analyse_prop(' (P\\/ Q) /\\ S')
test_no_exception(next(analyse_prop_count), lambda x : typecheck_formule(ret), 'analyse_prop' )
oracle = ('Et', (('Ou', (('Var', 'P'), ('Var', 'Q'))), ('Var', 'S')))
test_if(next(analyse_prop_count), ret == oracle, 'analyse_prop' )

ret = analyse_prop(' (P/\\ Q) /\\ S')
test_no_exception(next(analyse_prop_count), lambda x : typecheck_formule(ret), 'analyse_prop' )
oracle = ('Et', (('Et', (('Var', 'P'), ('Var', 'Q'))), ('Var', 'S')))
test_if(next(analyse_prop_count), ret == oracle, 'analyse_prop' )

ret = analyse_prop(' (P/\\ Q) /\\ S')
test_no_exception(next(analyse_prop_count), lambda x : typecheck_formule(ret), 'analyse_prop' )
oracle = ('Et', (('Et', (('Var', 'P'), ('Var', 'Q'))), ('Var', 'S')))
test_if(next(analyse_prop_count), ret == oracle, 'analyse_prop' )

ret = analyse_prop(' (P/\\ Q) ==> R')
test_no_exception(next(analyse_prop_count), lambda x : typecheck_formule(ret), 'analyse_prop' )
oracle = ('Implique', (('Et', (('Var', 'P'), ('Var', 'Q'))), ('Var', 'R')))
test_if(next(analyse_prop_count), ret == oracle, 'analyse_prop' )

test_exception(next(analyse_prop_count), lambda x : analyse_prop(' (P/\\ Q) = R'), 'analyse_prop' )
        
ret = analyse_prop(' Q ==> P ==> R')
test_no_exception(next(analyse_prop_count), lambda x : typecheck_formule(ret), 'analyse_prop' )
oracle = ('Implique', (('Var', 'Q'), ('Implique', (('Var', 'P'), ('Var', 'R')))))
test_if(next(analyse_prop_count), ret == oracle, 'analyse_prop' )

ret = analyse_prop(' (Q ==> P) ==> R')
test_no_exception(next(analyse_prop_count), lambda x : typecheck_formule(ret), 'analyse_prop' )
oracle = ('Implique', (('Implique', (('Var', 'Q'), ('Var', 'P'))), ('Var', 'R')))
test_if(next(analyse_prop_count), ret == oracle, 'analyse_prop' )

ret = analyse_prop(' P1 <==> Q')
test_no_exception(next(analyse_prop_count), lambda x : typecheck_formule(ret), 'analyse_prop' )
oracle = ('Equivalent', (('Var', 'P1'), ('Var', 'Q')))
test_if(next(analyse_prop_count), ret == oracle, 'analyse_prop' )

ret = analyse_prop(' ~P <==> Q ==> R')
test_no_exception(next(analyse_prop_count), lambda x : typecheck_formule(ret), 'analyse_prop' )
oracle = ('Equivalent', (('Non', ('Var', 'P')), ('Implique', (('Var', 'Q'), ('Var', 'R')))))
test_if(next(analyse_prop_count), ret == oracle, 'analyse_prop' )

ret = analyse_prop(' P ==> Q <==> ~P \\/Q')
test_no_exception(next(analyse_prop_count), lambda x : typecheck_formule(ret), 'analyse_prop' )
oracle = ('Equivalent',
 (('Implique', (('Var', 'P'), ('Var', 'Q'))),
  ('Ou', (('Non', ('Var', 'P')), ('Var', 'Q')))))
test_if(next(analyse_prop_count), ret == oracle, 'analyse_prop' )

test_if(next(analyse_prop_count), tautologie(analyse_prop(' P ==> Q <==> ~P \\/Q')), 'analyse_prop' )

test_if(next(analyse_prop_count), tautologie2(analyse_prop(' P ==> Q <==> ~P \\/Q')), 'analyse_prop' )

fin_test_fonction('analyse_prop')

# Modèle test
# debut_test_fonction('FONCTION')
# print(FONCTION.__doc__)
# FONCTION_count = gen_int_count()
# fin_test_fonction('FONCTION')

fin_test_module('syntax_formule')