bar = '*' * 80
minibar = '+' * 40

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def test_module(temps, module):
    s = '\t' + temps + ' Test module ' + module
    print(f"{bcolors.OKCYAN}{bar}{bcolors.ENDC}")
    print(f"{bcolors.OKCYAN}{s}{bcolors.ENDC}")
    print(f"{bcolors.OKCYAN}{bar}{bcolors.ENDC}")

def debut_test_module(module):
    test_module('Début', module)
     
def fin_test_module(module):
    test_module('Fin', module)

def test_fonction(temps, fonction):
    print(minibar + '\n\t' + temps + ' Test fonction ' + fonction +  '\n' + minibar )

def debut_test_fonction(fonction):
    test_fonction('Début', fonction)
     
def fin_test_fonction(fonction):
    test_fonction('Fin', fonction)

def test_result(num, fonction, description, result, color):
    des = "" if description == "" else " : " + description 
    s = result + " | Test " + str(num) + " " + fonction + des
    print(f"{color}{s}{bcolors.ENDC}")

def test_result_OK(num, fonction, description):
    test_result(num, fonction, description, 'OK', bcolors.OKGREEN)

def test_result_KO(num, fonction, description):
    test_result(num, fonction, description, 'KO', bcolors.FAIL)

def test_if(num, bool_exp, fonction, description=""):
    if bool_exp:
        test_result_OK(num, fonction, description)
    else:
        test_result_KO(num, fonction, description)

def test_ifnot(num, bool_exp, fonction, description=""):
    if not bool_exp:
        test_result_OK(num, fonction, description)
    else:
        test_result_KO(num, fonction, description)

def gen_int_count(num=1):
    while True:
        yield num
        num += 1

def test_exception(num, lambd, fonction, description=""):
    des = "" if description == "" else " : " + description 
    try:
        lambd(None)
        test_result_KO(num, fonction, des)
    except:
        test_result_OK(num, fonction, des)

def test_exception2(num, lambd, fonction, exception, description=""):
    try:
        ret = lambd(None)
        #sprint(ret)
        test_result_KO(num, fonction, description)
    except exception as err:
        s = format(err).split(',')
        s = s[0][1:]
        #print(s)
        des = "" if description == "" else description + " : "
        des = des + s 
        #des = description + " : {0}".format(err)
        test_result_OK(num, fonction, des)

def test_no_exception(num, lambd, fonction, description=""):
    des = "" if description == "" else " : " + description 
    try:
        lambd(None)
        test_result_OK(num, fonction, des)
    except:
        test_result_KO(num, fonction, des)

def test_compare_list(num, fonction, description, l0, l1):
    if len(l0) != len(l1):
        test_result_KO(num, fonction, description)
    for e0 in l0:
        if e0 not in l1:
            test_result_KO(num, fonction, description)
            return
    test_result_OK(num, fonction, description)