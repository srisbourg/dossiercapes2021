import sys
import os

sys.path.insert(0, os.path.join('..', 'lib'))

from pyticoq import *
from unit_test_common import *
from printer import *
from type_tactic import *
from syntax_tactic import SyntaxTacticError


debut_test_module('pyticoq')

# Tests pyticoq.introduction
debut_test_fonction('introduction')
print(introduction.__doc__)
introduction_count = gen_int_count()

formule = analyse_prop('(P==>Q)==>(P==>R)')
oracle = {'H1': ('Implique', (('Var', 'P'), ('Var', 'Q')))
          }, ('Implique', (('Var', 'P'), ('Var', 'R')))

test_if(next(introduction_count), introduction(
    ['H1'], {}, formule) == oracle, 'introduction')

ctx2, _ = introduction(['H1'], {}, formule)

test_exception(next(introduction_count), lambda x: introduction(
    ['H1'], ctx2, ('Vrai', None)), 'introduction')

fin_test_fonction('introduction')

# Tests pyticoq.conditions_apply
debut_test_fonction('conditions_apply')
print(conditions_apply.__doc__)
conditions_apply_count = gen_int_count()

phi = analyse_prop('P==>Q==>R')
formule = ('Var', 'R')
oracle = [('Var', 'P'), ('Var', 'Q')]

test_if(next(conditions_apply_count), conditions_apply(
    phi, formule) == oracle, 'conditions_apply')

phi = analyse_prop('~Q')

test_exception(next(conditions_apply_count),
               lambda x: conditions_apply(phi, formule), 'conditions_apply')

phi = analyse_prop('P==>Q==>R')
formule = ('Var', 'Z')

test_exception(next(conditions_apply_count),
               lambda x: conditions_apply(phi, formule), 'conditions_apply')

fin_test_fonction('conditions_apply')

# Tests pyticoq.apply_tactic
debut_test_fonction('apply_tactic')
print(apply_tactic.__doc__)
apply_tactic_count = gen_int_count()

# Help test fonction apply_tactic


def test_apply_tactic(sformContext, shypContext, sfomrGoal, tactic):
    formule_context = analyse_prop(sformContext)
    typecheck_formule(formule_context)
    context = {shypContext: formule_context}
    typecheck_context(context)
    formule_goal = analyse_prop(sfomrGoal)
    typecheck_formule(formule_goal)
    goal = mk_goal(context, formule_goal)
    typecheck_goal(goal)
    typecheck_tactic(tactic)
    return fresh_ident(), formule_context, context, formule_goal, goal, tactic


# exact
freshi, formule_context, context, formule_goal, goal, tactic = test_apply_tactic(
    'P==>Q', 'h1', 'P==>Q', ('exact', 'h1'))
oracle = []
test_if(next(apply_tactic_count), apply_tactic(
    tactic, goal, freshi) == oracle,  'apply_tactic', 'exact')

tactic = ('exact', 'h')
test_exception2(next(apply_tactic_count), lambda x: apply_tactic(tactic, goal, freshi),
                'apply_tactic', IdentInvalidError, 'exact IdentInvalidError')


# split Et
freshi, formule_context, context, formule_goal, goal, tactic = test_apply_tactic(
    'P==>Q', 'h1', 'P/\Q', ('split', None))
oracle = [mk_goal(context, ('Var', 'P')), mk_goal(context, ('Var', 'Q'))]
test_if(next(apply_tactic_count), apply_tactic(
    tactic, goal, freshi) == oracle,  'apply_tactic', 'split Et')

# split Equivalent
freshi, formule_context, context, formule_goal, goal, tactic = test_apply_tactic(
    'P==>Q', 'h1', 'P<==>Q', ('split', None))
oracle = [mk_goal(context, analyse_prop('P==>Q')),
          mk_goal(context, analyse_prop('Q==>P'))]
test_if(next(apply_tactic_count), apply_tactic(tactic, goal, freshi)
        == oracle,  'apply_tactic', 'split Equivalent')

# split TacticError
freshi, formule_context, context, formule_goal, goal, tactic = test_apply_tactic(
    'P==>Q', 'h1', '~P', ('split', None))
test_exception2(next(apply_tactic_count), lambda x: apply_tactic(tactic, goal, freshi),
                'apply_tactic', TacticError, 'split TacticError')

# case
freshi, formule_context, context, formule_goal, goal, tactic = test_apply_tactic(
    'P\/Q', 'h1', '~Z', ('case', 'h1'))
oracle = [mk_goal({'_hyp0': ('Var', 'P')} | context, formule_goal),
          mk_goal({'_hyp1': ('Var', 'Q')} | context, formule_goal)]
test_if(next(apply_tactic_count), apply_tactic(
    tactic, goal, freshi) == oracle,  'apply_tactic', 'case')

freshi, formule_context, context, formule_goal, goal, tactic = test_apply_tactic(
    'P/\Q', 'h1', '~Z', ('case', 'h1'))
test_exception(next(apply_tactic_count), lambda x: apply_tactic(tactic, goal, freshi),
                'apply_tactic', 'case TacticError')

freshi, formule_context, context, formule_goal, goal, tactic = test_apply_tactic(
    'P/\Q', 'h1', '~Z', ('case', 'h2'))
test_exception2(next(apply_tactic_count), lambda x: apply_tactic(tactic, goal, freshi),
                'apply_tactic', IdentInvalidError, 'case IdentInvalidError')

# decompose
freshi, formule_context, context, formule_goal, goal, tactic = test_apply_tactic(
    'P/\Q', 'h1', 'P<==>Q', ('decompose', 'h1'))
oracle = [mk_goal({'_hyp0': ('Var', 'P')} | {
                  '_hyp1': ('Var', 'Q')} | context, formule_goal)]
test_if(next(apply_tactic_count), apply_tactic(
    tactic, goal, freshi) == oracle,  'apply_tactic', 'decompose')

freshi, formule_context, context, formule_goal, goal, tactic = test_apply_tactic(
    'P/\Q', 'h1', 'P<==>Q', ('decompose', 'h2'))
test_exception2(next(apply_tactic_count), lambda x: apply_tactic(tactic, goal, freshi),
                'apply_tactic', IdentInvalidError, 'decompose IdentInvalidError')

freshi, formule_context, context, formule_goal, goal, tactic = test_apply_tactic(
    'P==>Q', 'h1', 'P<==>Q', ('decompose', 'h1'))
test_exception(next(apply_tactic_count), lambda x: apply_tactic(tactic, goal, freshi),
                'apply_tactic',  'decompose TacticError')

# left
freshi, formule_context, context, formule_goal, goal, tactic = test_apply_tactic(
    'P<==>Q', 'h1', 'P\/Q', ('left', None))
oracle = [mk_goal(context, ('Var', 'P'))]
test_if(next(apply_tactic_count), apply_tactic(
    tactic, goal, freshi) == oracle,  'apply_tactic', 'left')

freshi, formule_context, context, formule_goal, goal, tactic = test_apply_tactic(
    'P<==>Q', 'h1', 'P==>Q', ('left', None))
test_exception2(next(apply_tactic_count), lambda x: apply_tactic(tactic, goal, freshi),
                'apply_tactic', TacticError, 'left TacticError')

# right
freshi, formule_context, context, formule_goal, goal, tactic = test_apply_tactic(
    'P<==>Q', 'h1', 'P\/Q', ('right', None))
oracle = [mk_goal(context, ('Var', 'Q'))]
test_if(next(apply_tactic_count), apply_tactic(
    tactic, goal, freshi) == oracle,  'apply_tactic', 'right')

freshi, formule_context, context, formule_goal, goal, tactic = test_apply_tactic(
    'P<==>Q', 'h1', 'P==>Q', ('right', None))
test_exception2(next(apply_tactic_count), lambda x: apply_tactic(tactic, goal, freshi),
                'apply_tactic', TacticError, 'right TacticError')

# intro
freshi, formule_context, context, formule_goal, goal, tactic = test_apply_tactic(
    'P<==>Q', 'h1', 'P==>Q', ('intro', 'h2'))
oracle = [mk_goal(context | {'h2': ('Var', 'P')}, ('Var', 'Q'))]
test_if(next(apply_tactic_count), apply_tactic(
    tactic, goal, freshi) == oracle,  'apply_tactic', 'intro')

freshi, formule_context, context, formule_goal, goal, tactic = test_apply_tactic(
    'P<==>Q', 'h1', 'P==>Q', ('intro', 'h1'))
test_exception2(next(apply_tactic_count), lambda x: apply_tactic(tactic, goal, freshi),
                'apply_tactic', IdentInvalidError, 'intro IdentInvalidError')

freshi, formule_context, context, formule_goal, goal, tactic = test_apply_tactic(
    'P<==>Q', 'h1', 'P<==>Q', ('intro', 'h1'))
test_exception2(next(apply_tactic_count), lambda x: apply_tactic(tactic, goal, freshi),
                'apply_tactic', TacticError, 'intro TacticError')

# intros
freshi, formule_context, context, formule_goal, goal, tactic = test_apply_tactic(
    'P<==>Q', 'h1', 'P==>Q==>R', ('intros', ['h2', 'h3']))
oracle = [mk_goal(context | {'h2': ('Var', 'P'),
                  'h3': ('Var', 'Q')}, ('Var', 'R'))]
test_if(next(apply_tactic_count), apply_tactic(
    tactic, goal, freshi) == oracle,  'apply_tactic', 'intros')

freshi, formule_context, context, formule_goal, goal, tactic = test_apply_tactic(
    'P<==>Q', 'h1', 'P/\Q==>R', ('intros', ['h2', 'h3']))
test_exception2(next(apply_tactic_count), lambda x: apply_tactic(tactic, goal, freshi),
                'apply_tactic', TacticError, 'intros TacticError')

# apply
freshi, formule_context, context, formule_goal, goal, tactic = test_apply_tactic(
    'Q==>P==>R', 'h1', 'R', ('apply', 'h1'))
oracle = [mk_goal({'h1': analyse_prop('Q==>P==>R')}, ('Var', 'Q')),
          mk_goal({'h1': analyse_prop('Q==>P==>R')}, ('Var', 'P'))]
test_if(next(apply_tactic_count), apply_tactic(
    tactic, goal, freshi) == oracle,  'apply_tactic', 'apply')

freshi, formule_context, context, formule_goal, goal, tactic = test_apply_tactic(
    'Q==>P==>R', 'h1', 'R', ('apply', 'h2'))
test_exception2(next(apply_tactic_count), lambda x: apply_tactic(tactic, goal, freshi),
                'apply_tactic', IdentInvalidError, 'apply IdentInvalidError')

# absurd
freshi, formule_context, context, formule_goal, goal, tactic = test_apply_tactic(
    'Q==>P==>R', 'h1', 'R', ('absurd', ('Var', 'P')))
oracle = [mk_goal(context, ('Var', 'P')),
          mk_goal(context, ('Non', ('Var', 'P')))]
test_if(next(apply_tactic_count), apply_tactic(
    tactic, goal, freshi) == oracle,  'apply_tactic', 'absurd')


fin_test_fonction('apply_tactic')


# Tests pyticoq.step
debut_test_fonction('step')
print(step.__doc__)
step_count = gen_int_count()

#g = {'context':{'df': ('Vrai', None), 'd':('Faux', None)}, 'formule':('Vrai',None)}
#typecheck_goal(g)

g = mk_goal({}, analyse_prop('p==>q<==>(~p\/q)'))
typecheck_goal(g)

freshi = fresh_ident()

test_exception2(next(step_count ),\
    lambda x : step(g, freshi, 'fgfsgs'),\
        'step', SyntaxTacticError, description="SyntaxTacticError")

test_exception2(next(step_count ),\
    lambda x : step(g, freshi, 'intro _g'),\
        'step', SyntaxTacticError, description="SyntaxTacticError")

test_exception2(next(step_count ),\
    lambda x : step(g, freshi, 'intros h, _g'),\
        'step', SyntaxTacticError, description="SyntaxTacticError")

test_exception2(next(step_count ),\
    lambda x : step(g, freshi, 'intro df'),\
        'step', TacticError, description="TacticError")

#'split'
g = mk_goal({}, analyse_prop('p==>q<==>(~p\/q)'))
typecheck_goal(g)

oracle = [mk_goal({}, analyse_prop('(p==>q)==>(~p\/q)')),\
          mk_goal({}, analyse_prop('(~p\/q)==>(p==>q)')) ]

test_if(next(step_count ), step(g, freshi, 'split') == oracle, 'step', 'split (p==>q)<==>(~p\/q)')

def help_test_step(formule, oracle, tactic, freshi):
    g = mk_goal({}, analyse_prop(formule))
    typecheck_goal(g)
    test_if(next(step_count ), step(g, freshi, tactic) == oracle, 'step', tactic + ' ' + formule)

#, 'left', 'right'



#'exact', 'decompose', 'case', 'intro', 'apply'
#'absurd'
#'intros'

# TODO test chaqye tactique et exception


fin_test_fonction('step')





# Tests pyticoq.interactive_loop
debut_test_fonction('interactive_loop')
print(interactive_loop.__doc__)
interactive_loop_count = gen_int_count()

g = mk_goal({}, analyse_prop('(P==>Q)==>(Q==>R)==>P==>R'))
typecheck_goal(g)
tactics = ['intros h1, h2, h3', 'apply h2', 'apply h1', 'exact h3']

interactive_loop([g], tactics)


fin_test_fonction('interactive_loop')


# Tests pyticoq.pyticoq
debut_test_fonction('pyticoq')
print(pyticoq.__doc__)
pyticoq_count = gen_int_count()


pyticoq('(P==>Q)==>(Q==>R)==>P==>R',
        ['intros h1, h2, h3', 'apply h2', 'apply h1', 'exact h3'])

#pyticoq()

fin_test_fonction('pyticoq')


# Modèle test
# debut_test_fonction('FONCTION')
# print(FONCTION.__doc__)
# FONCTION_count = gen_int_count()
# fin_test_fonction('FONCTION')

fin_test_module('pyticoq')
